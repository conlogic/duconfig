..
 =============================================================================
 Title          : Generic configuration utilities

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-05-14

 Description    : Master file for the documentation.
 =============================================================================


==============================================================================
Documentation for ``duconfig``
==============================================================================

.. toctree::
   :maxdepth: 2

   overview
   usage

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
