..
 =============================================================================
 Title          : Generic configuration utilities

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2022-05-31

 Description    : Description of the user interface.
 =============================================================================


===================
 The user interface
===================


Loading ``duconfig``
====================

This package ``duconfig`` is used by importing its single module,
:py:mod:`duconfig`.  It describes itself as:

.. automodule:: duconfig


Expressive ``duconfig`` configuration for several file formats
==============================================================


The package ``duconfig`` provides, like already mentioned in :ref:`read_me`,
powerful configurations with expressive syntax for several file formats.
Actually, the JSON is supported by default, while the enhanced form of CONF
format requires package ``duconfigparser`` (`duconfigparser code`_ and
`duconfigparser documentation`_).


Some configuration file examples for ``duconfig``
-------------------------------------------------

Let us give two examples.  Both were already mentioned in :ref:`read_me`.

The first one consists of build configuration files for the *Shinobi* build
tool.  This build configuration is given both as a configuration file in JSON
format, and as a configuration file in CONF format.

First the example's JSON configuration file:

.. literalinclude:: ../examples/shinobi_config.json
   :language: json

and the example's CONF configuration file:

.. literalinclude:: ../examples/shinobi_config.conf
   :language: ini
   :start-at: [env.vars]

Since ``duconfig`` inherits from ``duipoldict`` it is based on the
possibility to call for its value functions from a given list, we show the
``CONFIG_FCTS`` of these functions for this example, together with their
documentation:

.. literalinclude:: ../examples/shinobi_config.py
   :language: python
   :start-at: # List of functions available for function call interpolation.
   :end-at: ]

.. autofunction:: examples.shinobi_config.join_paths

.. autofunction:: examples.shinobi_config.file_base

.. autofunction:: examples.shinobi_config.files_bases

.. autofunction:: examples.shinobi_config.add_files_ext

.. autofunction:: examples.shinobi_config.add_file_dir

.. autofunction:: examples.shinobi_config.add_files_dir

The second example is a configuration for a setup tool [#Ninja_setup]_.  This
example provides only a configuration file in CONF format:

.. literalinclude:: ../examples/ninja_setup.conf

This configuration file is, again, supplemented by the Python module that
defines functions to be used for function value interpolation.  For
completeness, let us show again the list ``CONFIG_FCTS`` of the example's
functions, and their documentation:

.. literalinclude:: ../examples/ninja_setup.py
   :language: python
   :start-at: # List of functions available for function call interpolation.
   :end-at: ]

.. autofunction:: examples.ninja_setup.calc_app_name

.. autofunction:: examples.ninja_setup.calc_app_version


Structure of configuration files for ``duconfig``
-------------------------------------------------

1. Irrespective of its format, every configuration files consists of named
   sections where every sections contains options being associated with
   values.  Section names and options have to be strings; values can be either
   strings or list of strings.  Please note that list values in configuration
   files with CONF format presuppose the ``duconfigparser`` package.

   For instance, in our example configuration for Shinobi, section ``rules``
   contains an option ``cc`` whose value is string ``"gcc -o $out $ccopts
   $in"``.  But option ``srcs`` in section ``paths`` has a list value with the
   two string elements ``"%(add_file_dir %{paths.srcdir}%, calc_fib.c)%"`` and
   ``"%(add_file_dir %{paths.srcdir}%, fibonacci.c)%"``:

   + For the JSON configuration file:

     .. literalinclude:: ../examples/shinobi_config.json
        :language: json
        :lines: 1,12-15,28-39,40
        :emphasize-lines: 2,3,5,6,8-11,17

   + For the CONF configuration file:

     .. literalinclude:: ../examples/shinobi_config.conf
        :language: ini
        :lines: 18-21,36-44
        :emphasize-lines: 1,3,5,9-12

2. Sections can be nested, i.e. a section can contain, beside option value
   pairs other sections as subsections.  For JSON configuration files this is
   achieved by its nesting syntax; but for CONF configuration files
   subsections use compound section names.

   For instance, in our example configuration for Shinobi, section ``env``
   has, beside option ``env.ninja_cmd``, subsection ``vars``, and section
   ``goals`` consists in its entirety of subsections ``cc`` and ``lnk``.

   + For the JSON configuration file:

     .. literalinclude:: ../examples/shinobi_config.json
        :language: json
        :lines: 1,2-7,16-27,40
        :emphasize-lines: 2,3,5,7,8,9,13,14,18,19

   + For the CONF configuration file:

     .. literalinclude:: ../examples/shinobi_config.conf
        :language: ini
        :lines: 4-10,24-34
        :emphasize-lines: 1,8,14

3. The values for options are either strings, or lists of strings.  Whereas
   lists are already supported by standard JSON, for the CONF format list
   value support is implemented by the ``duconfigparser`` package.  Please
   note that the standard syntax for lists is similar both for the JSON and
   the CONF format.  Please note that, because of CONF format limitations,
   lists cannot be nested, an all continuation lines of list value in a CONF
   file must be indented - including the list end separator.

   For instance, in our example configuration for Shinobi, option ``srcs`` in
   section ``paths`` has a list with two elements as value, and option
   ``headerdirs`` in the same section has a list with one element as value.

   + For the JSON configuration file:

     .. literalinclude:: ../examples/shinobi_config.json
        :language: json
        :lines: 1,28-39,40
        :emphasize-lines: 4-7,8

   + For the CONF configuration file:

     .. literalinclude:: ../examples/shinobi_config.conf
        :language: ini
        :lines: 36-45
        :emphasize-lines: 5-8,10

4. Moreover, every configuration file can use value interpolation as provided
   by the ``duipoldict`` package.  Let us remind that ``duipoldict`` provides
   two types of value interpolation, meaning we can refer by special
   references within parameter values to other values.  There are two types of
   value interpolation:

   + *item interpolation*: refer, by using key references, to values for other
     parameters of the configuration;

   + *function call interpolation*; refer, by using function call references,
     to the value of a function call, where the functions that can be used for
     such function calls are given by a list.

   For details about value interpolation, please consult section "Value
   interpolation in duipoldict dictionaries" in the `duipoldict usage`_
   chapter of the ``duipoldict`` documentation.

   To provide examples for both types of value interpolation let use again use
   our Shinobi example configuration: the value of option ``ins`` in section
   ``goals.cc`` is item reference ``'%{paths.srcdir}%'`` that refer to the
   value of option ``srcdir`` in section ``paths``.  On the other hand, option
   ``out`` in the same section ``goals.cc`` uses by the function call
   reference ``'%(file_base $in)%'`` the value calling unary function
   :py:func:`file_base` for (the value of) `$in`, that is the path for an
   input file for a ``goals.cc`` goal.

   + For the JSON configuration file:

     .. literalinclude:: ../examples/shinobi_config.json
        :language: json
        :lines: 1,16,17,19,20,21,27,40
        :emphasize-lines: 4,5

   + For the CONF configuration file:

     .. literalinclude:: ../examples/shinobi_config.conf
        :language: ini
        :lines: 24-28
        :emphasize-lines: 4,5

5. Finally, configuration files in CONF format has an universal line
   continuation mechanism: every line can marked by a ``\`` an its end to be
   continued by the next line.  For better readability continuation lines can
   be indented by any amount of whitespace that is generously
   ignored. [#linecont]_

   To see this mechanism in action, have a look at our Shinobi CONF
   configuration example with the two continued lines emphasized:

   .. literalinclude:: ../examples/shinobi_config.conf
      :language: ini
      :lines: 46-49
      :emphasize-lines: 2,3


Working with ``duconfig`` objects
=================================

Module :py:mod:`duconfig` - the sole module of the ``duconfig`` package -
mainly defines :py:class:`duconfig.Config` objects to save configurations:

.. autoclass:: duconfig.Config

Please note that :py:class:`Config` objects are specific
:py:class:`duipoldict.InterpolDict` objects.  Like the latter, the values of a
configuration are internally stored using an abstract syntax that is
independent from the concrete syntax used for value interpolation and for list
values in CONF configuration files.

Let us explain the purpose of the arguments ``oth_beg`` / ``oth_end`` to
specify other begin / end delimiters.  As mentioned in the documentation,
these parameters are passed to :py:class:`duconfigparser.ConfigParser` objects
that are used to read a CONF configuration file.  One can use these parameters
to prevent a :py:mod:`duconfigparser.ConfigParser` to confuse a list separator
with an argument separator used in function calls within configuration files.
This can be illustrated by the setup configuration example.  In this example,
parameter values can contain strings that represent function calls - like
this:

.. literalinclude:: ../examples/ninja_setup.conf
   :language: ini
   :start-at: # Instructions for the `install` step.
   :end-before: # Parameters for `install` step.
   :emphasize-lines: 3,4

Here the second list element contains the string representation of a function
call with two arguments, and their separator ``,`` must not be confused with a
``,`` as list element separator to end the first line.  To prevent this one
can specify ``'('`` / ``')'`` as other begin / end delimiters:

.. code-block:: python

   # Import functions used for value interpolation in Setup configuration.
   from examples.ninja_setup import CONFIG_FCTS as SETUP_CONFIG_FCTS

   import duconfig

   # Create a :py:class:`Config` object ``cfg`` for setup configuration.
   setup_cfg = duconfig.Config(fcts=SETUP_CONFIG_FCTS,
                               oth_begs=['('], oth_ends=[')'])

Now the argument expressions ``(...)`` for function call strings in parameter
values are treated as atomic values by :py:mod:`duconfigparser.ConfigParser`
when scanning for list values. [#fct_itp]_

By the way, :py:mod:`duconfig` defines an own exception used for its own error
handling:

.. autoclass:: duconfig.ConfigError

Freshly initialized :py:class:`duconfig.Config` objects do not store any
configuration data.  In most cases configuration is read from configuration
files, either in JSON or CONF format.  For this the following method is used:

.. automethod:: duconfig.Config.read_file

Let us show the creation of :py:class:`duconfig.Config` objects and reading of
a configuration file for our example mentioned above.  First we create a
:py:class:`duconfig.Config` object using the standard syntax that is used both
for the JSON and CONF example files shown above:

.. code-block:: python

   # Import functions used for value interpolation in Shinobi configuration.
   from examples.shinobi_config import CONFIG_FCTS as SNOB_CONFIG_FCTS

   import duconfig

   # Create a :py:class:`Config` object ``cfg_std`` (standard syntax).
   cfg_std = duconfig.Config(fcts=SNOB_CONFIG_FCTS)

Now either the JSON or the CONF configuration file could be read:

.. code-block:: python

   # Read the example Shinobi JSON configuration file (standard syntax).
   cfg_std.read_file('example/shinobi_config.json')

.. code-block:: python

   # Read the example Shinobi CONF configuration file (standard syntax).
   cfg_std.read_file('example/shinobi_config.conf')

Please note that format of a configuration file is chosen according to the
file extension: files with a ``.json`` file extension are treated as JSON
configuration file, and for CONF configuration file a ``.conf`` file extension
is expected.

To demonstrate the usage of an alternative concrete syntax, we take the
following variant of the example Shinobi CONF configuration file.  It provides
the same configuration for Shinobi and uses the CONF format, too, but a
different concrete syntax:

.. literalinclude:: ../examples/shinobi_alt_config.conf
   :language: ini
   :start-at: [env:vars]

It could be read into a :py:class:`Config` object like this:

.. code-block:: python

   # Import functions used for value interpolation in Shinobi configuration.
   from examples.shinobi_config import CONFIG_FCTS as SNOB_CONFIG_FCTS

   import duconfig

   # Create a :py:class:`Config` object ``cfg_alt`` (alternative syntax).
   cfg_alt = duconfig.Config(
     key_beg='{', key_end='}', key_sep=':', fct_beg='(', fct_end=')',
     arg_sep='|', list_beg='', list_end='', list_sep='\n', line_cont='$',
     fcts=SNOB_CONFIG_FCTS)

   # Read the example Shinobi CONF configuration file (standard syntax).
   cfg_alt.read_file('example/shinobi_alt_config.conf')

Please note that the configuration data for all three Shinobi example
configuration files is the same.  In particular, the internal abstract
representation of this configuration data is the same in all three cases.

The remaining API methods of :py:class:`duconfig.Config` are all inherited
from :py:class:`duipoldict.InterpolDict` - with one difference: the
:py:class:`duconfig.Config` variants only raise :py:exc:`duconfig.ConfigError`
exceptions.  For completeness, we include their documentation strings here,
with some example of their usage added.  For more details please consult the
documentation of :py:class:`duipoldict.InterpolDict`.

First there are tow methods to import and export the configuration of a
:py:class:`duconfig.Config` object as ordinary dictionary with raw values
either in concrete and abstract syntax:

.. automethod:: duconfig.Config.read_dict

.. automethod:: duconfig.Config.write_dict

Next there are two methods to get value of a configuration for its parameters.

.. automethod:: duconfig.Config.__getitem__

.. automethod:: duconfig.Config.get

Please note again that :py:meth:`duconfig.Config.__getitem__`, i.e. the method
implementing the ``[]`` operator for :py:class:`duconfig.Config` always use
the default value variant of the py:class:`duconfig.Config` object.  The
:py:meth:`duconfig.Config.get` method is more flexible in this respect - you
can always overwrite the default value variant by using the optional arguments
``raw`` and ``con`` of :py:meth:`duconfig.Config.get`.

Let use exemplify this using our running Shinobi configuration example:

.. code-block:: python

   # Import functions used for value interpolation in Shinobi configuration.
   from examples.shinobi_config import CONFIG_FCTS as SNOB_CONFIG_FCTS

   import duconfig

   # Create a :py:class:`Config` object ``cfg_con`` using raw concrete values
   # by default (standard syntax).
   cfg_con = duconfig.Config(fcts=SNOB_CONFIG_FCTS)
   # Create a :py:class:`Config` object ``cfg_abs`` using raw abtract values
   # by default (standard syntax).
   cfg_abs = duconfig.Config(fcts=SNOB_CONFIG_FCTS, con=False)
   # Create a :py:class:`Config` object ``cfg_exp`` using expanded values by
   # default (standard syntax).
   cfg_exp = duconfig.Config(fcts=SNOB_CONFIG_FCTS, raw=False)

   # Read the example Shinobi CONF configuration file (standard syntax).
   cfg_con.read_file('example/shinobi_config.conf')
   cfg_abs.read_file('example/shinobi_config.conf')
   cfg_exp.read_file('example/shinobi_config.conf')

   # Some different ways to get the raw concrete value of parameter ``out`` in
   # section ``goals.cc``.
   cfg_con['goals.cc.out']
   cfg_con.get('goals.cc.out')
   cfg_abs.get('goals.cc.out', con=True)
   cfg_exp.get('goals.cc.out', raw=True)

   # Some different ways to get the raw abstract value of parameter ``out`` in
   # section ``goals.cc``.
   cfg_abs['goals.cc.out']
   cfg_abs.get('goals.cc.out')
   cfg_con.get('goals.cc.out', con=False)
   cfg_exp.get('goals.cc.out', raw=True, con=False)

   # Some different ways to get the expanded value of parameter ``out`` in
   # section ``goals.cc``.
   cfg_exp['goals.cc.out']
   cfg_exp.get('goals.cc.out')
   cfg_con.get('goals.cc.out', raw=False)
   cfg_abs.get('goals.cc.out', raw=False)


Like for :py:class:`duipoldict.InterpolDict` dictionaries, we can also expand
configuration values by the following method:

.. automethod:: duconfig.Config.expand_value

For details of the value expansion that is done by value interpolation please
consult section "Value interpolation in duipoldict dictionaries" in the
`duipoldict usage`_ chapter of the ``duipoldict`` documentation.

Let us show this for our running Shinobi configuration example for a whole
section:

.. code-block:: python

   # Import functions used for value interpolation in Shinobi configuration.
   from examples.shinobi_config import CONFIG_FCTS as SNOB_CONFIG_FCTS

   import duconfig

   # Create a :py:class:`Config` object ``cfg``.
   cfg = duconfig.Config(fcts=SNOB_CONFIG_FCTS)

   # Get the configuration section ``goals.cc`` as dictionary.
   # - Using raw concrete values.
   goals_cc_con = cfg['goals.cc']
   # - Using raw abstract values.
   goals_cc_abs = cfg.get('goals.cc', con=False)
   # - Using expanded values.
   goals_cc_exp = cfg.get('goals.cc', raw=False)

   # Three ways to get configuration section ``goals.cc`` as dictionary with
   # expanded values.
   cfg.expand_value(goals_cc_con)
   cfg.expand_value(goals_cc_abs)
   cfg.expand_value(goals_cc_exp)

Please note that the last example also shows that :py:meth:`expand_value` is
idempotent: expanding already expanded configuration values does nothing.

Let us show at least one example for the different error handling of the
:py:class:`Config` methods compared with their
:py:class:`duipoldict.InterpolDict` counterparts.  Reusing the ``cfg``
configuration object from the last example, the following call

.. code-block:: python

   cfg.expand_value(13)

raises a :py:exc:`duconfig.ConfigError` instead of a
:py:exc:`duipoldict.InterpolationError`.

Finally, let us mention a helper function from :py:mod:`duconfig` that may be
interesting in its own right.  Here it is used to transform
:py:class:`duconfigparser.ConfigParser` objects into a dictionary.

.. autofunction:: duconfig.configparser_to_dict


Using external configuration files
==================================

A Python package may need a configuration file outside this very package - for
instance to customize the Python package without the need to change the
package itself.  A common way to have such a configuration file for a Python
package outside of this package is to use a user configuration file, i.e. a
configuration file below the XDG [#xdg]_ configuration directory of the
respective user.  The path of this directory is given by the following
function defined in :py:mod:`duconfig`:

.. autofunction:: duconfig.user_config_dir

It can be useful to search in list of directories, possibly including the
user's directory for configuration files, for a configuration file
given by its name:

.. autofunction:: duconfig.search_config_file

One use case for this would be a Python package that has a builtin
configuration file, and this builtin configuration file is used as fallback if
there is user configuration file with the same name.


.. [#Ninja_setup] Actually, this configuration is used by a private setup tool
                  of mine to set up build tool `Ninja
                  <https://ninja-build.org>`_ from source.

.. [#linecont] This universal line continuation mechanism is already
               implemented in the ``duconfigparser`` package.  By the way, the
               line end marker string for continued lines can be customized.

.. [#fct_itp] A thoughtful reader may note that the same problem arises for
              functions with more than one argument used for function value
              interpolation if the standard argument separator value (argument
              `arg_sep`) is used.  Therefore the begin (argument `fct_beg`) /
              end (argument `fct_end`) delimiters for function call references
              automatically passed as other begin / end delimiters to the
              :py:mod:`duconfigparser.ConfigParser` object that is used to
              handle CONF configuration files.

.. [#xdg] This user configuration directory is specified, beside other
          directories, in the `XDG Base Directory Specification
          <https://specifications.freedesktop.org/basedir-spec/>`_.


.. _duconfigparser code: https://gitlab.com/conlogic/duconfigparser
.. _duconfigparser documentation: https://duconfigparser.readthedocs.io
.. _duipoldict code: https://gitlab.com/conlogic/duipoldict
.. _duipoldict documentation: https://duipoldict.readthedocs.io
.. _duipoldict usage: https://duipoldict.readthedocs.io/en/latest/usage.html

..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
