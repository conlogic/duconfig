# Print the n-th Fibonacci number.
# Configuration CONF file for Shinobi.

[env.vars]
# Ensure that the compiler finds the C header file(s).
CPATH = %(join_paths %{paths.headerdirs}%)%

[env]
# Ninja command to be used.
ninja_cmd = ninja

[vars]
# Options for compiling C implementation files.
ccopts = -c -std=c99 -Wall -pedantic
# Options for linking object files.
lnkopts = -fPIC

[rules]
# Rule to compile C implementation files to object files.
cc = gcc -o $out $ccopts $in
# Rule to link object files to a executable file.
lnk = gcc -o $out $lnkopts $in

[goals.cc]
# Goal to compile all C implementation files.
rule = cc
ins = %{paths.srcs}%
out = %{paths.bindir}%/%(file_base $in)%.o

[goals.lnk]
# Goal to link the object files for the interactive player.
rule = lnk
in = %{paths.objs}%
out = %{paths.bindir}%/%{paths.exe}%

[paths]
# Directory with C implementation files.
srcdir = c
# All C implementation files.
srcs = [
  %(add_file_dir %{paths.srcdir}%, calc_fib.c)%,
  %(add_file_dir %{paths.srcdir}%, fibonacci.c)%
  ]
# Directories with C header files.
headerdirs = [include]
# All object names.
objnames = %(\
  add_files_ext %(files_bases %{paths.srcs}%)%, o\
)%
# Directory with binary / object files.
bindir = bin
# All object files.
objs = %(add_files_dir %{paths.bindir}%, %{paths.objnames}%)%
# Base name for executable.
exe = fibonacci
