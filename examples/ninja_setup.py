'''
Set up Ninja from source.
Functions for value interpolation in the configuration of the setup tool.
'''


import pathlib


def calc_app_name():
    '''
    Calculate the name for the application's source in the current working
    directory.  It is assumed that the application's name and its version is
    separated by ``'-'``, and the application name is made lowercase.

    :return: calculated application name.
    '''
    dname = pathlib.Path.cwd().name

    sep_idx = dname.rfind('-')
    if sep_idx < 0:
        # Name version separator was not found -> take the whole name as
        # application name.
        app_name_str = dname
    else:
        # Name version separator was found -> application name is the part
        # before the separator.
        app_name_str = dname[:sep_idx]

    # Make the application name lowercase.
    app_name = app_name_str.lower()

    return app_name


def calc_app_version(nr_parts=None):
    '''
    Calculate the version for the application's source in the current working
    directory.  It is assumed that the application's name and its version is
    separated by ``'-'``, and that the version components are separated by
    ``'.'``.

    :return: calculated application version.
    '''
    dname = pathlib.Path.cwd().name

    sep_idx = dname.rfind('-')
    if sep_idx < 0:
        # Name version separator was not found -> take the application version
        # as empty..
        app_vers_str = ''
    else:
        # Name version separator was found -> application version is rightmost
        # part after the separator.
        app_vers_str = dname[sep_idx + 1:]

    if nr_parts is not None and app_vers_str != '':
        app_vers_parts = app_vers_str.split('.')
        app_vers_used_parts = app_vers_parts[:int(nr_parts)]
        app_vers = '.'.join(app_vers_used_parts)
    else:
        app_vers = app_vers_str

    return app_vers


# List of functions available for function call interpolation.
CONFIG_FCTS = [
    calc_app_name,
    calc_app_version
]
