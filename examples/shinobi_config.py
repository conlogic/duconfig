'''
Print the n-th Fibonacci number.
Functions used for function call interpolation in the configuration files.
'''


import pathlib
import os


def join_paths(paths):
    '''
    :return: the paths from list `paths` joined to a string using the path
             separator.

    Examples:

    >>> join_paths(['foo/bar'])
    'foo/bar'

    >>> join_paths(['foo', '/bar', 'bar/baz'])
    'foo:/bar:bar/baz'
    '''
    paths_str = os.pathsep.join(paths)

    return paths_str


def file_base(fpath):
    '''
    :return: file base of filepath `fpath`, i.e. `fpath` with its directory
             part and extension stripped.

    Examples:

    >>> file_base('foo/bar.baz')
    'bar'

    >>> file_base('bar.baz')
    'bar'

    >>> file_base('foo/bar')
    'bar'
    '''
    fpath = pathlib.Path(fpath)

    return fpath.stem


def files_bases(fpaths):
    '''
    :return: list of file bases for the filepaths in list `fpaths`.

    Example:

    >>> files_bases(['foo/bar1.baz', 'bar2.baz', 'foo/bar3'])
    ['bar1', 'bar2', 'bar3']
    '''
    fbases = [file_base(f) for f in fpaths]

    return fbases


def add_files_ext(fpaths, fext):
    '''
    :return: list of filepaths `fpaths` as strings with file extension `fext`
             added to each of them.

    Example:

    >>> add_files_ext(['foo1/bar1', 'foo2.bar2'], 'baz')
    ['foo1/bar1.baz', 'foo2.bar2.baz']
    '''
    fpaths_with_ext = []
    for f in fpaths:
        old_f = pathlib.Path(f)
        old_fext_f = old_f.suffix
        new_fext_f = old_fext_f + os.extsep + fext
        new_f = old_f.with_suffix(new_fext_f)
        fpaths_with_ext.append(str(new_f))

    return fpaths_with_ext


def add_file_dir(dpath, fpath):
    '''
    :return: filepath `fpath` as string with (additional) dictionary path
             `dpath` added.

    Examples:

    >>> add_file_dir('/foo', 'bar')
    '/foo/bar'

    >>> add_file_dir('foo', 'bar/baz')
    'foo/bar/baz'

    >>> add_file_dir('/foo', '/bar/baz')
    '/bar/baz'

    >>> add_file_dir('foo/bar1', 'bar2/baz')
    'foo/bar1/bar2/baz'
    '''
    dpath = pathlib.Path(dpath)

    return str(dpath / fpath)


def add_files_dir(dpath, fpaths):
    '''
    :return: list of filepaths `fpaths` as strings with (additional) directory
             path `dpath` added to each of them.

    Example:

    >>> add_files_dir('foo', ['bar', 'bar/baz', '/bar/baz'])
    ['foo/bar', 'foo/bar/baz', '/bar/baz']
    '''
    fpaths_with_dir = [add_file_dir(dpath, f) for f in fpaths]

    return fpaths_with_dir


# List of functions available for function call interpolation.
CONFIG_FCTS = [
    join_paths,
    file_base,
    files_bases,
    add_files_ext,
    add_file_dir,
    add_files_dir
]


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
