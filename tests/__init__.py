# ============================================================================
# Title          : Generic configuration utilities
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2022-05-31
#
# Description    : See documentation string below.
# ============================================================================

'''
Init module for the unit test package of package ``duconfig``.
'''


# This turns the :py:mod:`tests` directory into a Python package.


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
