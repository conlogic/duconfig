# ============================================================================
# Title          : Generic configuration utilities
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2022-05-31
#
# Description    : See documentation string below.
# ============================================================================

'''
Unit tests for module :py:mod:`duconfig`.
'''


import unittest
import pathlib
import shutil
import tempfile
import json
import sys
import configparser
import os

import duipoldict
try:
    import duconfigparser as dcp
    has_conf_support = dcp.version >= '2.0' and dcp.version < '3.0'
except ModuleNotFoundError:
    has_conf_support = False

import duconfig


# Functions and classes
# =====================


def conv_tuple_to_str_key(key_tuple, key_sep):
    '''
    Convert a flat dictionary key given as tuple `key_tuple` to corresponding
    string key using key separator `key_sep`.
    '''
    if isinstance(key_tuple, tuple):
        k_str = ''
        for e in key_tuple:
            if len(k_str) > 0 and key_sep is not None:
                k_str += key_sep
            k_str += str(e)
    else:
        k_str = key_tuple

    return k_str


def conv_nested_to_flat_tuple_dict(nest_dict):
    '''
    :return: nested dictionary `nested_dict` converted to a flat dictionary
             with tuple keys and the same values for corresponding keys.
    '''
    flat_dict = {}
    for k in nest_dict.keys():
        if isinstance(nest_dict[k], dict):
            flat_dict_k = conv_nested_to_flat_tuple_dict(nest_dict[k])
            for flat_k in flat_dict_k.keys():
                new_flat_k = (k,) + flat_k
                flat_dict[new_flat_k] = flat_dict_k[flat_k]
        else:
            flat_dict[(k,)] = nest_dict[k]

    return flat_dict


def conv_nested_to_flat_str_dict(nest_dict, key_sep='.'):
    '''
    :return: nested dictionary `nested_dict` converted to a flat dictionary
             with structured string keys using key separator `key_sep` and the
             same values for corresponding keys.
    '''
    flat_tuple_dict = conv_nested_to_flat_tuple_dict(nest_dict)
    flat_str_dict = {
        conv_tuple_to_str_key(k, key_sep): v
        for (k, v) in flat_tuple_dict.items()
    }

    return flat_str_dict


def as_AbstractValue(dct):
    '''
    If dictionary `dct` represents a :py:class:`duipoldict.AbstractValue`
    encoded, by :py:class:`duipoldict.AbstractValueEncoder`, to be dumped to
    JSON, `dct` is converted back to a respective
    :py:class:`duipoldict.AbstractValue` object.

    :return: a :py:class:`AbstractValue` encoded by `dct`, if `dct` stems from
             encoding of such an object, and `dct` itself otherwise,
    '''
    if set(dct.keys()) == {'category', 'attrs'}:
        cat_dct = dct['category']
        attrs_dct = dct['attrs']
        if cat_dct == 'Str':
            # Initialization is special for abstract string values.
            obj_for_dct = duipoldict.AbstractStr(attrs_dct['parts'])
        elif cat_dct == 'KeyRef':
            obj_for_dct = duipoldict.AbstractKeyRef(**attrs_dct)
        elif cat_dct == 'FctCallRef':
            obj_for_dct = duipoldict.AbstractFctCallRef(**attrs_dct)
        else:
            obj_for_dct = dct
    else:
        obj_for_dct = dct

    return obj_for_dct


class AbstractValueEncoder(json.JSONEncoder):
    '''
    Variant of :py:class:`json.JSONEncoder` to handle
    :py:class:`duipoldict.AbstractValue`s, too.
    '''
    def default(self, obj):
        '''
        A variant of :py:meth:`json.JSONEncoder.default` extended for
        :py:class:`duipoldict.AbstractValue`s.
        '''
        if isinstance(obj, duipoldict.AbstractValue):
            enc_obj = {'category': obj.category}
            enc_obj['attrs'] = obj.__dict__
        else:
            enc_obj = super().default(obj)

        return enc_obj


def read_json_file(fpath, object_hook=None):
    '''
    Read JSON file with path `fpath` into a dictionary.

    A not-``None`` function `object_hook` is called with the result of any
    object literal decoded as dictionary, and the result of this call will be
    used instead of this literal.

    :return: content of JSON file with path `fpath` read into a dictionary.
    '''
    with open(fpath, 'rt') as fobj:
        content = json.load(fobj, object_hook=object_hook)

    return content


def write_json_file(content, fpath, cls=None, indent=1):
    '''
    Write content `content` to JSON file with path `fpath` using indentation
    level `indent`.

    A not-``None`` class `cls` is used as JSON encoder instead of the standard
    one.
    '''
    dpath = pathlib.Path(fpath).parent
    dpath.mkdir(parents=True, exist_ok=True)

    with open(fpath, 'wt') as fobj:
        json.dump(content, fobj, cls=cls, indent=indent)


def mk_test_data():
    '''
    Create test data from the example configuration.

    :return: list of all generated test data files.
    '''
    data_fpaths = []

    tc = base_duconfig_TC()
    tc.setUp()

    # Nested dictionary with abstract raw values.
    # - Shinobi configuration.
    cfg_snob = duconfig.Config(raw=True, con=False)
    cfg_snob.read_file(tc.snob_json_fpath)
    snob_nest_abs_dict = cfg_snob.write_dict()
    write_json_file(
        snob_nest_abs_dict, tc.snob_nest_abs_fpath, cls=AbstractValueEncoder)
    data_fpaths.append(tc.snob_nest_abs_fpath)
    # - Setup configuration.
    cfg_setup = duconfig.Config(raw=True, con=False, **tc.setup_syntax)
    cfg_setup.read_file(tc.setup_conf_fpath)
    setup_nest_abs_dict = cfg_setup.write_dict()
    write_json_file(
        setup_nest_abs_dict, tc.setup_nest_abs_fpath,
        cls=AbstractValueEncoder)
    data_fpaths.append(tc.setup_nest_abs_fpath)

    # Flat dictionary with concrete raw values.
    # - Shinobi configuration.
    snob_nest_con_dict = read_json_file(tc.snob_json_fpath)
    snob_flat_con_dict = conv_nested_to_flat_str_dict(snob_nest_con_dict)
    write_json_file(snob_flat_con_dict, tc.snob_flat_con_fpath)
    data_fpaths.append(tc.snob_flat_con_fpath)

    # Flat dictionary with abstract raw values.
    # - Shinobi configuration.
    snob_flat_abs_dict = conv_nested_to_flat_str_dict(snob_nest_abs_dict)
    write_json_file(
        snob_flat_abs_dict, tc.snob_flat_abs_fpath, cls=AbstractValueEncoder)
    data_fpaths.append(tc.snob_flat_abs_fpath)

    # Flat dictionary with expanded values.
    # - Shinobi configuration.
    cfg_snob = duconfig.Config(fcts=tc.snob_fcts)
    cfg_snob.read_file(tc.snob_json_fpath)
    snob_nest_exp_dict = cfg_snob.expand_value(dict(cfg_snob))
    snob_flat_exp_dict = conv_nested_to_flat_str_dict(snob_nest_exp_dict)
    write_json_file(snob_flat_exp_dict, tc.snob_flat_exp_fpath)
    data_fpaths.append(tc.snob_flat_exp_fpath)

    return data_fpaths


class base_duconfig_TC(unittest.TestCase):
    '''
    Base class for :py:mod:`duconfig` tests.
    '''
    def setUp(self):
        # Directory path of this very module.
        this_dpath = pathlib.Path(__file__).parent
        # Path for the directory containing the examples.
        self.examples_dpath = this_dpath.parent / 'examples'
        # Path for the directory with test data.
        self.test_data_dpath = this_dpath / 'data'
        # Path for the base test directory.
        self.test_base_dpath = \
            pathlib.Path(tempfile.gettempdir()) / 'duconfig-tests'

        # Paths for configuration example files.
        # - Shinobi configuration, JSON format.
        self.snob_json_fpath = self.examples_dpath / 'shinobi_config.json'
        # - Shinobi configuration, CONF format.
        self.snob_conf_fpath = self.examples_dpath / 'shinobi_config.conf'
        # - Setup configuration (CONF format only).
        self.setup_conf_fpath = self.examples_dpath / 'ninja_setup.conf'

        # Paths for test data files representing configuration example files,
        # together with dictionaries with these test data.
        # - Shinobi configuration, with raw concrete values for a nested
        #   dictionary.
        self.snob_nest_con_dict = read_json_file(self.snob_json_fpath)
        # - Shinobi configuration, with raw abstract values for a nested
        #   dictionary.
        self.snob_nest_abs_fpath = self.test_data_dpath \
            / 'shinobi_config_nest_abs.json'
        if self.snob_nest_abs_fpath.is_file():
            self.snob_nest_abs_dict = read_json_file(
                self.snob_nest_abs_fpath, object_hook=as_AbstractValue)
        # - Setup configuration, with raw abstract values for a nested
        #   dictionary.
        self.setup_nest_abs_fpath = self.test_data_dpath \
            / 'ninja_setup_nest_abs.json'
        if self.setup_nest_abs_fpath.is_file():
            self.setup_nest_abs_dict = read_json_file(
                self.setup_nest_abs_fpath, object_hook=as_AbstractValue)
        # - Shinobi configuration, with raw concrete values for a flat
        #   dictionary.
        self.snob_flat_con_fpath = self.test_data_dpath \
            / 'shinobi_config_flat_con.json'
        if self.snob_flat_con_fpath.is_file():
            self.snob_flat_con_dict = read_json_file(self.snob_flat_con_fpath)
        # - Shinobi configuration, with raw abstract values for a flat
        #   dictionary.
        self.snob_flat_abs_fpath = self.test_data_dpath \
            / 'shinobi_config_flat_abs.json'
        if self.snob_flat_abs_fpath.is_file():
            self.snob_flat_abs_dict = read_json_file(
                self.snob_flat_abs_fpath, object_hook=as_AbstractValue)
        # - Shinobi configuration, with expanded values for a flat dictionary.
        self.snob_flat_exp_fpath = self.test_data_dpath \
            / 'shinobi_config_flat_exp.json'
        if self.snob_flat_exp_fpath.is_file():
            self.snob_flat_exp_dict = read_json_file(self.snob_flat_exp_fpath)

        # Ensure that a clean base test directory exists.
        if self.test_base_dpath.is_dir():
            shutil.rmtree(self.test_base_dpath)
        self.test_base_dpath.mkdir()

        # List with functions available for function call interpolation for
        # the configuration example.
        # - Extend load path.
        self._orig_sys_path = sys.path
        sys.path.append(str(self.examples_dpath))
        # - Interpolation functions for Shinobi configuration.
        import shinobi_config as snob_fcts
        self.snob_fcts = snob_fcts.CONFIG_FCTS
        # - Interpolation functions for setup configuration.
        import ninja_setup as setup_fcts
        self.setup_fcts = setup_fcts.CONFIG_FCTS

        # Additional syntax parameters.
        # - Setup configuration.
        self.setup_syntax = {'oth_begs': ['('], 'oth_ends': [')']}

        # Specific example parameters.
        # - Shinobi configuration parameter with value without value
        #   references.
        self.snob_raw_val_param = 'paths.exe'
        # - Shinobi configuration parameter with value with value references.
        self.snob_exp_val_param = 'paths.objnames'


    def tearDown(self):
        # Restore original load path.
        sys.path = self._orig_sys_path


class Config_read_dict_TC(base_duconfig_TC):
    '''
    Tests for method :py:meth:`duconfig.Config.read_dict` of
    :py:class:`duconfig.Config`.
    '''


    def test_parse_error(self):
        '''
        Test with a dictionary causing a parse error.
        '''
        con_dict = {'foo': '%{bar'}
        with self.assertRaises(duconfig.ConfigError):
            cfg = duconfig.Config()
            cfg.read_dict(con_dict)


    def test_std(self):
        '''
        A standard smoke test using the example.
        '''
        cfg = duconfig.Config()
        con_dict = read_json_file(self.snob_json_fpath)
        cfg.read_dict(con_dict)
        cfg_res = cfg.data
        self.assertDictEqual(cfg_res, self.snob_nest_abs_dict)


class Config_write_dict_TC(base_duconfig_TC):
    '''
    Tests for method :py:meth:`duconfig.Config.write_dict` of
    :py:class:`duconfig.Config`.
    '''


    def test_format_error(self):
        '''
        Test with a configuration causing a formatting error.
        '''
        cfg = duconfig.Config()
        cfg.data = {'key': 13}
        with self.assertRaises(duconfig.ConfigError):
            cfg.write_dict()


    def test_std(self):
        '''
        A standard smoke test using the example.
        '''
        cfg = duconfig.Config()
        cfg.read_file(self.snob_json_fpath)
        cfg.data = self.snob_nest_abs_dict
        exp_data = cfg.write_dict()
        self.assertDictEqual(exp_data, self.snob_nest_con_dict)


class Config_read_file_TC(base_duconfig_TC):
    '''
    Tests for method :py:meth:`duconfig.Config.read_file` of
    :py:class:`duconfig.Config`.
    '''
    def setUp(self):
        super().setUp()

        # Configuration CONF example file with alternative syntax.
        # - Path for the Shinobi configuration file.
        self.snob_alt_conf_fpath = \
            self.examples_dpath / 'shinobi_alt_config.conf'
        # - Alternative syntax.
        self.alt_syntax = {
            'key_beg': '{', 'key_end': '}', 'key_sep': ':',
            'fct_beg': '(', 'fct_end': ')', 'arg_sep': '|',
            'list_beg': '', 'list_end': '', 'list_sep': '\n',
            'line_cont': '$'
        }

    def test_no_file(self):
        '''
        Test with a missing configuration file.
        '''
        with self.assertRaises(duconfig.ConfigError):
            cfg = duconfig.Config()
            cfg.read_file('foo.json')


    def test_unknown_format(self):
        '''
        Test with a configuration file with an unknown format.
        '''
        with self.assertRaises(duconfig.ConfigError):
            cfg = duconfig.Config()
            cfg.read_file(__file__)


    def test_json_error(self):
        '''
        Test with a JSON configuration invalid file.
        '''
        cfg_fpath = self.test_base_dpath / 'foo.json'
        shutil.copy(__file__, cfg_fpath)
        cfg = duconfig.Config(raw=True)
        with self.assertRaises(duconfig.ConfigError):
            cfg.read_file(cfg_fpath)
        cfg_fpath.unlink()


    def test_conf_error(self):
        '''
        Test with a CONF configuration invalid file.
        '''
        cfg_fpath = self.test_base_dpath / 'foo.conf'
        shutil.copy(__file__, cfg_fpath)
        cfg = duconfig.Config(raw=True)
        with self.assertRaises(duconfig.ConfigError):
            cfg.read_file(cfg_fpath)
        cfg_fpath.unlink()


    def test_json_non_utf8_error(self):
        '''
        Test with a non-UTF-8 JSON file.
        '''
        # Create non-UTF-8 JSON file.
        dct_bin = b'{"foo": "b\xf6se"}'
        cfg_fpath = self.test_base_dpath / 'foo.json'
        with open(cfg_fpath, 'wb') as cfg_fobj:
            cfg_fobj.write(dct_bin)
        # Try to read the JSON file.
        cfg = duconfig.Config(raw=True)
        with self.assertRaises(duconfig.ConfigError):
            cfg.read_file(cfg_fpath)
        cfg_fpath.unlink()


    def test_conf_non_utf8_error(self):
        '''
        Test with a non-UTF-8 CONF file.
        '''
        # Create non-UTF-8 CONF file.
        comment_bin = b'# b\xf6se'
        cfg_fpath = self.test_base_dpath / 'foo.conf'
        with open(cfg_fpath, 'wb') as cfg_fobj:
            cfg_fobj.write(comment_bin)
        # Try to read the CONF file.
        cfg = duconfig.Config(raw=True)
        with self.assertRaises(duconfig.ConfigError):
            cfg.read_file(cfg_fpath)
        cfg_fpath.unlink()


    def test_json_file(self):
        '''
        Test for a JSON configuration file.
        '''
        cfg = duconfig.Config(raw=True)
        cfg.read_file(self.snob_json_fpath)
        cfg_res = cfg.data
        self.assertDictEqual(cfg_res, self.snob_nest_abs_dict)


    def test_conf_file(self):
        '''
        Test for a CONF configuration file.
        '''
        cfg = duconfig.Config(raw=True)
        cfg.read_file(self.snob_conf_fpath)
        cfg_res = cfg.data
        self.assertDictEqual(cfg_res, self.snob_nest_abs_dict)


    def test_alt_conf_file(self):
        '''
        Test for a CONF configuration file with alternative syntax.
        '''
        cfg = duconfig.Config(raw=True, **self.alt_syntax)
        cfg.read_file(self.snob_alt_conf_fpath)
        cfg_res = cfg.data
        self.assertDictEqual(cfg_res, self.snob_nest_abs_dict)


    def test_oth_delims(self):
        '''
        Test for a CONF configuration file that needs other delimiters.
        '''
        cfg = duconfig.Config(raw=True, **self.setup_syntax)
        cfg.read_file(self.setup_conf_fpath)
        cfg_res = cfg.data
        self.assertDictEqual(cfg_res, self.setup_nest_abs_dict)


class Config_get_TC(base_duconfig_TC):
    '''
    Tests for method :py:meth:`duconfig.Config.get` of
    :py:class:`duconfig.Config`.
    '''


    def test_interpol_error(self):
        '''
        Test causing an interpolation error.
        '''
        param = 'param'
        data = {param: '%(foo)%'}
        cfg = duconfig.Config(raw=False)
        cfg.read_dict(data)
        with self.assertRaises(duconfig.ConfigError):
            cfg.get(param)


    def test_key_error(self):
        '''
        Test causing a key error.
        '''
        data = {'foo': 'bar'}
        cfg = duconfig.Config()
        cfg.read_dict(data)
        with self.assertRaises(duconfig.ConfigError):
            cfg.get('bar')


    def test_value_error(self):
        '''
        Test causing a value error.
        '''
        param = 'param'
        data = {'foo': {'bar': ['baz1', 'baz2']}, param: '%{foo.bar.baz1}%'}
        cfg = duconfig.Config(raw=False)
        cfg.read_dict(data)
        with self.assertRaises(duconfig.ConfigError):
            cfg.get(param)


    def test_std_std(self):
        '''
        Test default initialization + method defaults.
        '''
        cfg = duconfig.Config(fcts=self.snob_fcts)
        cfg.read_file(self.snob_json_fpath)
        val_res = cfg.get(self.snob_exp_val_param)
        val_exp = self.snob_flat_con_dict[self.snob_exp_val_param]
        self.assertEqual(val_res, val_exp)


    def test_std_con(self):
        '''
        Test default initialization + method force concrete raw values.
        '''
        cfg = duconfig.Config(fcts=self.snob_fcts)
        cfg.read_file(self.snob_json_fpath)
        val_res = cfg.get(self.snob_exp_val_param, con=True)
        val_exp = self.snob_flat_con_dict[self.snob_exp_val_param]
        self.assertEqual(val_res, val_exp)


    def test_std_abs(self):
        '''
        Test default initialization + method force abstract raw values.
        '''
        cfg = duconfig.Config(fcts=self.snob_fcts)
        cfg.read_file(self.snob_json_fpath)
        val_res = cfg.get(self.snob_exp_val_param, con=False)
        val_exp = self.snob_flat_abs_dict[self.snob_exp_val_param]
        self.assertEqual(val_res, val_exp)


    def test_std_exp(self):
        '''
        Test default initialization + force expanded values.
        '''
        cfg = duconfig.Config(fcts=self.snob_fcts)
        cfg.read_file(self.snob_json_fpath)
        val_res = cfg.get(self.snob_exp_val_param, raw=False)
        val_exp = self.snob_flat_exp_dict[self.snob_exp_val_param]
        self.assertEqual(val_res, val_exp)


    def test_con_std(self):
        '''
        Test default concrete raw values + method defaults.
        '''
        cfg = duconfig.Config(fcts=self.snob_fcts, con=True)
        cfg.read_file(self.snob_json_fpath)
        val_res = cfg.get(self.snob_exp_val_param)
        val_exp = self.snob_flat_con_dict[self.snob_exp_val_param]
        self.assertEqual(val_res, val_exp)


    def test_con_abs(self):
        '''
        Test default concrete raw values + method force abstract raw values.
        '''
        cfg = duconfig.Config(fcts=self.snob_fcts, con=True)
        cfg.read_file(self.snob_json_fpath)
        val_res = cfg.get(self.snob_exp_val_param, con=False)
        val_exp = self.snob_flat_abs_dict[self.snob_exp_val_param]
        self.assertEqual(val_res, val_exp)


    def test_con_exp(self):
        '''
        Test default concrete raw values + method force expanded values.
        '''
        cfg = duconfig.Config(fcts=self.snob_fcts, con=True)
        cfg.read_file(self.snob_json_fpath)
        val_res = cfg.get(self.snob_exp_val_param, raw=False)
        val_exp = self.snob_flat_exp_dict[self.snob_exp_val_param]
        self.assertEqual(val_res, val_exp)


    def test_abs_std(self):
        '''
        Test default abstract raw values + method defaults.
        '''
        cfg = duconfig.Config(fcts=self.snob_fcts, con=False)
        cfg.read_file(self.snob_json_fpath)
        val_res = cfg.get(self.snob_exp_val_param)
        val_exp = self.snob_flat_abs_dict[self.snob_exp_val_param]
        self.assertEqual(val_res, val_exp)


    def test_abs_con(self):
        '''
        Test default abstract raw values + method force concrete raw values.
        '''
        cfg = duconfig.Config(fcts=self.snob_fcts, con=False)
        cfg.read_file(self.snob_json_fpath)
        val_res = cfg.get(self.snob_exp_val_param, con=True)
        val_exp = self.snob_flat_con_dict[self.snob_exp_val_param]
        self.assertEqual(val_res, val_exp)


    def test_abs_exp(self):
        '''
        Test default abstract raw values + method force expanded values.
        '''
        cfg = duconfig.Config(fcts=self.snob_fcts, con=False)
        cfg.read_file(self.snob_json_fpath)
        val_res = cfg.get(self.snob_exp_val_param, raw=False)
        val_exp = self.snob_flat_exp_dict[self.snob_exp_val_param]
        self.assertEqual(val_res, val_exp)


    def test_exp_std(self):
        '''
        Test default expanded values + method defaults.
        '''
        cfg = duconfig.Config(fcts=self.snob_fcts, raw=False)
        cfg.read_file(self.snob_json_fpath)
        val_res = cfg.get(self.snob_exp_val_param)
        val_exp = self.snob_flat_exp_dict[self.snob_exp_val_param]
        self.assertEqual(val_res, val_exp)


    def test_exp_con(self):
        '''
        Test default expanded values + method force concrete raw values.
        '''
        cfg = duconfig.Config(fcts=self.snob_fcts, raw=False)
        cfg.read_file(self.snob_json_fpath)
        val_res = cfg.get(self.snob_exp_val_param, raw=True, con=True)
        val_exp = self.snob_flat_con_dict[self.snob_exp_val_param]
        self.assertEqual(val_res, val_exp)


    def test_exp_abs(self):
        '''
        Test default expanded values+ method force abstract raw values.
        '''
        cfg = duconfig.Config(fcts=self.snob_fcts, raw=False)
        cfg.read_file(self.snob_json_fpath)
        val_res = cfg.get(self.snob_exp_val_param, raw=True, con=False)
        val_exp = self.snob_flat_abs_dict[self.snob_exp_val_param]
        self.assertEqual(val_res, val_exp)


class Config_getitem_TC(base_duconfig_TC):
    '''
    Tests for method :py:meth:`duconfig.Config.__getitem__` of
    :py:class:`duconfig.Config`.
    '''


    def test_interpol_error(self):
        '''
        Test causing an interpolation error.
        '''
        param = 'param'
        data = {param: '%(foo)%'}
        cfg = duconfig.Config(raw=False)
        cfg.read_dict(data)
        with self.assertRaises(duconfig.ConfigError):
            cfg[param]


    def test_key_error(self):
        '''
        Test causing a key error.
        '''
        data = {'foo': 'bar'}
        cfg = duconfig.Config()
        cfg.read_dict(data)
        with self.assertRaises(duconfig.ConfigError):
            cfg['bar']


    def test_value_error(self):
        '''
        Test causing a value error.
        '''
        param = 'param'
        data = {'foo': {'bar': ['baz1', 'baz2']}, param: '%{foo.bar.baz1}%'}
        cfg = duconfig.Config(raw=False)
        cfg.read_dict(data)
        with self.assertRaises(duconfig.ConfigError):
            cfg[param]


    def test_std(self):
        '''
        Test default initialization.
        '''
        cfg = duconfig.Config(fcts=self.snob_fcts)
        cfg.read_file(self.snob_json_fpath)
        val_res = cfg[self.snob_exp_val_param]
        val_exp = self.snob_flat_con_dict[self.snob_exp_val_param]
        self.assertEqual(val_res, val_exp)


    def test_con(self):
        '''
        Test default concrete raw values.
        '''
        cfg = duconfig.Config(fcts=self.snob_fcts, con=True)
        cfg.read_file(self.snob_json_fpath)
        val_res = cfg[self.snob_exp_val_param]
        val_exp = self.snob_flat_con_dict[self.snob_exp_val_param]
        self.assertEqual(val_res, val_exp)


    def test_abs(self):
        '''
        Test default abstract raw values.
        '''
        cfg = duconfig.Config(fcts=self.snob_fcts, con=False)
        cfg.read_file(self.snob_json_fpath)
        val_res = cfg[self.snob_exp_val_param]
        val_exp = self.snob_flat_abs_dict[self.snob_exp_val_param]
        self.assertEqual(val_res, val_exp)


    def test_exp(self):
        '''
        Test default expanded values.
        '''
        cfg = duconfig.Config(fcts=self.snob_fcts, raw=False)
        cfg.read_file(self.snob_json_fpath)
        val_res = cfg[self.snob_exp_val_param]
        val_exp = self.snob_flat_exp_dict[self.snob_exp_val_param]
        self.assertEqual(val_res, val_exp)


class Config_expand_value_TC(base_duconfig_TC):
    '''
    Tests for method :py:meth:`duconfig.Config.expand_value` of
    :py:class:`duconfig.Config`.
    '''


    def test_parse_error(self):
        '''
        Test a raw concrete value causing a parse error.
        '''
        val = '%{paths.exe'
        cfg = duconfig.Config(fcts=self.snob_fcts)
        cfg.read_file(self.snob_json_fpath)
        with self.assertRaises(duconfig.ConfigError):
            cfg.expand_value(val)


    def test_key_error(self):
        '''
        Test a raw concrete value causing a key error.
        '''
        val = '%{paths.foo}%'
        cfg = duconfig.Config(fcts=self.snob_fcts)
        cfg.read_file(self.snob_json_fpath)
        with self.assertRaises(duconfig.ConfigError):
            cfg.expand_value(val)


    def test_val_error(self):
        '''
        Test a raw concrete value causing a value error.
        '''
        val = '%{foo.bar.baz1}%'
        data = {'foo': {'bar': ['baz1', 'baz2']}, 'key': val}
        cfg = duconfig.Config()
        cfg.read_dict(data)
        with self.assertRaises(duconfig.ConfigError):
            cfg.expand_value(val)


    def test_con_val_no_ref(self):
        '''
        Test a raw concrete value without value references.
        '''
        param = self.snob_raw_val_param
        val = self.snob_flat_con_dict[param]
        cfg = duconfig.Config(fcts=self.snob_fcts)
        cfg.read_file(self.snob_json_fpath)
        res_val = cfg.expand_value(val)
        exp_val = self.snob_flat_exp_dict[param]
        self.assertEqual(res_val, exp_val)


    def test_abs_val_no_ref(self):
        '''
        Test a raw abstract value without value references.
        '''
        param = self.snob_raw_val_param
        val = self.snob_flat_abs_dict[param]
        cfg = duconfig.Config(fcts=self.snob_fcts)
        cfg.read_file(self.snob_json_fpath)
        res_val = cfg.expand_value(val)
        exp_val = self.snob_flat_exp_dict[param]
        self.assertEqual(res_val, exp_val)


    def test_con_val_with_ref(self):
        '''
        Test a raw abstract value with value references.
        '''
        param = self.snob_exp_val_param
        val = self.snob_flat_con_dict[param]
        cfg = duconfig.Config(fcts=self.snob_fcts)
        cfg.read_file(self.snob_json_fpath)
        res_val = cfg.expand_value(val)
        exp_val = self.snob_flat_exp_dict[param]
        self.assertEqual(res_val, exp_val)


    def test_abs_val_with_ref(self):
        '''
        Test a raw abstract value with value references.
        '''
        param = self.snob_exp_val_param
        val = self.snob_flat_abs_dict[param]
        cfg = duconfig.Config(fcts=self.snob_fcts)
        cfg.read_file(self.snob_json_fpath)
        res_val = cfg.expand_value(val)
        exp_val = self.snob_flat_exp_dict[param]
        self.assertEqual(res_val, exp_val)


class configparser_to_dict_TC(base_duconfig_TC):
    '''
    Tests for function :py:func:`duconfig.configparser_to_dict`.
    '''
    def setUp(self):
        super().setUp()

        # Possible section name separator.
        self.sect_sep = '.'

        # Prepare a :py:class:`ConfigParser` with a configuration string
        # read.  One section of this configuration can be interpreted as
        # nested using section name separator :py:attr:`sect_sep`.  Please
        # note that there are only string values.
        self.simple_cp = configparser.ConfigParser()
        conf_str = '''
        [top.nest]
        opt = top_nest_val
        [top]
        opt = top_val
        [top_only]
        opt = top_only_val
        '''
        self.simple_cp.read_string(conf_str)


    def test_simple_no_sect_sep(self):
        '''
        Test simple configuration and no section name separator.
        '''
        res_dict = duconfig.configparser_to_dict(
            self.simple_cp, sect_sep=None)
        exp_dict = {
            'top': {'opt': 'top_val'},
            'top.nest': {'opt': 'top_nest_val'},
            'top_only': {'opt': 'top_only_val'},
        }
        self.assertDictEqual(res_dict, exp_dict)


    def test_simple_with_sect_sep(self):
        '''
        Test simple configuration and a section name separator.
        '''
        res_dict = duconfig.configparser_to_dict(
            self.simple_cp, sect_sep=self.sect_sep)
        exp_dict = {
            'top': {
                'nest': {'opt': 'top_nest_val'},
                'opt': 'top_val'},
            'top_only': {'opt': 'top_only_val'},
        }
        self.assertDictEqual(res_dict, exp_dict)


    def test_example_conf(self):
        '''
        Test example CONF configuration + ``duconfigparser`` available.
        '''
        if has_conf_support:
            # We need to register the function call reference delimiters as
            # other delimiters to split list value strings properly.
            cp = dcp.ConfigParser(
                interpolation=None, oth_begs=['%('], oth_ends=[')%'],
                line_cont='\\')
            cp.read(self.snob_conf_fpath)
            res_dict = duconfig.configparser_to_dict(
                cp, sect_sep=self.sect_sep)
            exp_dict = read_json_file(self.snob_json_fpath)
            self.assertDictEqual(res_dict, exp_dict)


class user_config_dir_TC(base_duconfig_TC):
    '''
    Tests for function :py:func:`duconfig.user_config_dir`.
    '''
    def test_no_xdg_config_home_var(self):
        '''
        Test with environment variable ``XDG_CONFIG_HOME`` not set.
        '''
        if 'XDG_CONFIG_HOME' in os.environ.keys():
            del os.environ['XDG_CONFIG_HOME']
        exp_dpath = pathlib.Path('~/.config').expanduser()
        res_dpath = duconfig.user_config_dir()
        self.assertEqual(res_dpath, exp_dpath)


    def test_xdg_config_home_var(self):
        '''
        Test with environment variable ``XDG_CONFIG_HOME`` set.
        '''
        xdg_cfg_dpath = self.test_base_dpath / 'config'
        exp_dpath = xdg_cfg_dpath
        os.environ['XDG_CONFIG_HOME'] = str(xdg_cfg_dpath)
        res_dpath = duconfig.user_config_dir()
        self.assertEqual(res_dpath, exp_dpath)


class search_config_file(base_duconfig_TC):
    '''
    Tests for function :py:func:`duconfig.search_config_file`.
    '''
    def setUp(self):
        super().setUp()
        # Fake a XDG user configuration directory below the test base
        # directory.
        self.xdg_cfg_dpath = self.test_base_dpath / 'config'
        os.environ['XDG_CONFIG_HOME'] = str(self.xdg_cfg_dpath)
        # Use the example CONF file as test configuration file.
        self.cfg_fname = self.snob_conf_fpath.name
        self.oth_cfg_dpath = self.snob_conf_fpath.parent
        self.oth_cfg_fpath = self.snob_conf_fpath
        self.xdg_cfg_fpath = self.xdg_cfg_dpath / self.cfg_fname


    def tearDown(self):
        # Ensure that the faked XDG user configuration directory vanishes
        # after every test.
        if self.xdg_cfg_dpath.is_dir():
            shutil.rmtree(self.xdg_cfg_dpath)


    def test_no_dpaths(self):
        '''
        Test with empty list of directory paths.
        '''
        res_fpath = duconfig.search_config_file(self.cfg_fname, [])
        self.assertIsNone(res_fpath)


    def test_oth_dpath_ex(self):
        '''
        Test with other directory path for existing directory.
        '''
        exp_fpath = self.oth_cfg_fpath
        res_fpath = duconfig.search_config_file(
            self.cfg_fname, [self.oth_cfg_dpath])
        self.assertEqual(res_fpath, exp_fpath)


    def test_user_dpath_noex(self):
        '''
        Test with user directory path for existing directory.
        '''
        res_fpath = duconfig.search_config_file(self.cfg_fname, ['user'])
        self.assertIsNone(res_fpath)


    def test_user_dpath_ex(self):
        '''
        Test with user directory path for existing directory.
        '''
        self.xdg_cfg_dpath.mkdir()
        shutil.copy(self.snob_conf_fpath, self.xdg_cfg_fpath)
        exp_fpath = self.xdg_cfg_fpath
        res_fpath = duconfig.search_config_file(self.cfg_fname, ['user'])
        self.assertEqual(res_fpath, exp_fpath)


    def test_user_oth_dpaths_2nd(self):
        '''
        Test with user before other directory path, 2nd directory exists.
        '''
        exp_fpath = self.oth_cfg_fpath
        res_fpath = duconfig.search_config_file(
            self.cfg_fname, ['user', self.oth_cfg_dpath])
        self.assertEqual(res_fpath, exp_fpath)


    def test_user_oth_dpaths_1st(self):
        '''
        Test with user before other directory path, 1st directory exists.
        '''
        self.xdg_cfg_dpath.mkdir()
        shutil.copy(self.snob_conf_fpath, self.xdg_cfg_fpath)
        exp_fpath = self.xdg_cfg_fpath
        res_fpath = duconfig.search_config_file(
            self.cfg_fname, ['user', self.oth_cfg_dpath])
        self.assertEqual(res_fpath, exp_fpath)


    def test_oth_user_dpaths_both(self):
        '''
        Test with other before user directory path, both directories exist.
        '''
        self.xdg_cfg_dpath.mkdir()
        shutil.copy(self.snob_conf_fpath, self.xdg_cfg_fpath)
        exp_fpath = self.oth_cfg_fpath
        res_fpath = duconfig.search_config_file(
            self.cfg_fname, [self.oth_cfg_dpath, 'user'])
        self.assertEqual(res_fpath, exp_fpath)


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
