#!/usr/bin/env python3
# ============================================================================
# Title          : Generic configuration utilities
#
# Classification : Python script
#
# Author         : Dirk Ullrich
#
# Date           : 2022-05-31
#
# Description    : See documentation string below.
# ============================================================================


import pathlib
import sys
this_dpath = pathlib.Path(__file__).parent
pkg_dpath = this_dpath.parent.parent
# Inserting before the standard Python module paths is important since there
# may exist other Python packages named :py:mod:`tests` or :py:mod`examples`
# in one of these paths.
sys.path.insert(0, str(pkg_dpath))

import tests.test_duconfig


# Functions
# =========


def main():
    print("Generating test data for 'duipoldict'...")
    fpaths = tests.test_duconfig.mk_test_data()
    print("Generating test data for 'duipoldict' generated:")
    for f in fpaths:
        print(f"  {f}")


# Doing
# =====


if __name__ == '__main__':
    main()


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
