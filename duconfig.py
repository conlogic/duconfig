# ============================================================================
# Title          : Generic configuration utilities
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2022-05-31
#
# Description    : See documentation string below.
# ============================================================================

'''
Generic configuration objects and utilities.
'''


import abc
import pathlib
import os
import json
import configparser

import duipoldict
import dubasiclog


# Parameters
# ==========

# Version of the very package.
version = '5.0'

# Support for CONF configuration files.
try:
    import duconfigparser as dcp
    has_conf_support = dcp.version >= '2.3' and dcp.version < '3.0'
except ModuleNotFoundError:
    has_conf_support = False


# Functions and classes
# =====================


# For uniform logging we use the module-global logging facilities from package
# ``dubasiclog``.
enable_debug_log = dubasiclog.enable_debug_log
disable_debug_log = dubasiclog.disable_debug_log
debug_is_enabled = dubasiclog.debug_is_enabled
debug_log = dubasiclog.debug_log


def user_config_dir():
    '''
    :return: path for user's XDG configuration directory.
    '''
    # Get the user's XDG configuration directory.
    xdg_cfg_dpath = os.environ.get("XDG_CONFIG_HOME")
    if xdg_cfg_dpath is None:
        xdg_cfg_dpath = pathlib.Path('~', '.config').expanduser()
    else:
        xdg_cfg_dpath = pathlib.Path(xdg_cfg_dpath)

    return xdg_cfg_dpath


def search_config_file(cfg_fname, dpaths):
    '''
    Search for the configuration file with name `cfg_fname` in every directory
    whose path is element of list `dpath`.

    An element ``'user'`` has special meaning: it represents the user's
    configuration directory.

    :return: the path of the first configuration file with name `cfg_fname`
             found, if there is one, and ``None`` otherwise.
    '''
    xdg_cfg_dpath = user_config_dir()

    cfg_fpath_found = None
    for d in dpaths:
        if d == 'user':
            cfg_dpath_d = xdg_cfg_dpath
        else:
            cfg_dpath_d = pathlib.Path(d)
        cfg_fpath_d = cfg_dpath_d / cfg_fname
        if cfg_fpath_d.is_file():
            cfg_fpath_found = cfg_fpath_d
            break

    return cfg_fpath_found


class Config(duipoldict.InterpolDict):
    '''
    Mapping object to store configuration read from appropriate sources with
    value interpolation allowed for its values.

    The following types of value interpolation can be used:

    + item interpolation: references to other keys within the configuration
      will be replaced by their corresponding values.

    + function call interpolation: references to a function call using the
      name of a function available for interpolation are replaced by the value
      of the respective function call.

    For details about value interpolation please consult the documentation of
    :py:mod:`duipoldict` from the package with the same name.

    The keyword arguments are either passed to the initialization of
    underlying :py:class:`duipoldict.InterpolDict` object or to the
    initialization of a :py:class:`duconfigparser.ConfigParser` object to
    handle configuration files of the CONF format.  Actually,

    + Parameters passed to :py:class:`duipoldict.InterpolDict` initialization:

      `key_beg`, `key_sep`, `key_end` / `fct_beg`, `arg_sep`, `fct_end`
      determine the syntax of key references / function call references, and
      `fcts` the list of available functions; all used for value
      interpolation.  Finally, parameters `raw` and `con` determine the
      default variant for values.

      Please lookup the details in the documentation of
      :py:mod:`duipoldict`.

    + Parameters passed to :py:class:`duconfigparser.ConfigParser`
      initialization:

      `list_beg`, `list_sep` and `list_end` determine the handling of
      list value strings in configuration files of the CONF format, sequences
      `oth_begs` / `oth_ends` for other begin / end delimiters, and
      `line_cont` the end marker for lines continued by the next line.

      Please lookup the details in the documentation of
      :py:mod:`duconfigparser`.  Beside this, the `key_sep` parameter is also
      used as section name component separator.
    '''
    def __init__(self, fcts=[], raw=True, con=True,
                 key_beg='%{', key_end='}%', key_sep='.',
                 fct_beg='%(', fct_end=')%', arg_sep=',',
                 list_beg='[', list_sep=',', list_end=']',
                 oth_begs=[], oth_ends=[], line_cont='\\'):
        # - Arguments for :py:class:`duconfigparser.ConfigParser`.  They are
        #   saved, for later use.
        self.duconfigparser_kwargs = {
            'list_beg': list_beg, 'list_end': list_end, 'list_sep': list_sep,
            'line_cont': line_cont}

        # Start with an empty dictionary.
        super().__init__(
            {}, fcts, raw, con,
            key_beg, key_end, key_sep,
            fct_beg, fct_end, arg_sep)

        # We also add, beside `oth_begs` / `oth_ends, the value reference
        # begin / end delimiters as other begin / end delimiters for our
        # :py:class:`duconfigparser.ConfigParser`.
        all_oth_begs = oth_begs.copy()
        all_oth_ends = oth_ends.copy()
        all_oth_begs.extend([self.syntax['key_beg'], self.syntax['fct_beg']])
        all_oth_ends.extend([self.syntax['key_end'], self.syntax['fct_end']])
        self.duconfigparser_kwargs['oth_begs'] = all_oth_begs
        self.duconfigparser_kwargs['oth_ends'] = all_oth_ends

        # The key separator is also used as separator in section names.
        self._sect_sep = self._parser.syntax['key_sep']


    def read_dict(self, data):
        '''
        Read configuration from mapping `data`.

        :raise: a :py:exc:`ConfigError` if reading of `data` fails.
        '''
        with duipoldict_error_wrapper():
            super().read_dict(data)


    def write_dict(self, con=None):
        '''
        Write current configuration as dictionary.

        A not-``None`` switch `con` forces whether this dictionary has
        concrete values (for a ``True`` value) or abstract ones (for a
        ``False`` value).  For a ``None`` value of `con` the default is used.

        :return: current configuration as dictionary.

        :raise: a :py:exc:`ConfigError` if formatting of the values for
                current concrete syntax fails.
        '''
        with duipoldict_error_wrapper():
            dct = super().write_dict(con)

        return dct


    def read_file(self, fpath):
        '''
        Read configuration file with path `fpath`, if the file's format can be
        handled, and store the resulting configuration.

        :raise: a :py:exc:`ConfigError` if reading fails in any way.
        '''
        fpath = pathlib.Path(fpath)
        if not fpath.is_file():
            raise ConfigError(
                f"No readable configuration file at:\n  {fpath}")

        fext_with_sep = fpath.suffix
        fext = fext_with_sep.lstrip(os.extsep).lower()
        try:
            if fext == 'json':
                self._read_json_file(fpath)
            elif fext == 'conf':
                self._read_conf_file(fpath)
            else:
                # We add information for the available file formats and their
                # extensions.
                msg = f"Format for configuration file at\n  {fpath}\n"
                msg += 'is wrong.  The following formats are allowed:'
                msg += "\n- JSON format (file name '*.json')"
                if has_conf_support:
                    msg += "\n- CONF format (file name '*.conf')"
                raise ConfigError(msg)
        except UnicodeDecodeError as e:
            msg = f"The following encoding error has occurred:\n  {e}\n"
            raise ConfigError(msg, loc=str(fpath))


    def _read_json_file(self, json_fpath):
        '''
        Read JSON configuration file with path `json_fpath`, and store the
        resulting configuration.

        :raise: a :py:exc:`ConfigError` if the configuration file could not be
                read as a JSON file.
        '''
        with open(json_fpath, 'r') as json_fobj, json_error_wrapper():
            json_cfg = json.load(json_fobj)

        self.read_dict(json_cfg)


    def _read_conf_file(self, conf_fpath):
        '''
        Read CONF configuration file with path `conf_fpath`, and store the
        resulting configuration.

        Please note that this method is only available if the module
        :py:mod:`duconfigparser` could be loaded.

        :raise: a :py:exc:`ConfigError` if :py:mod:`duconfigparser` could not
                be loaded, or if the configuration file could not be read as a
                CONF file.
        '''
        if not has_conf_support:
            raise ConfigError("Module 'duconfigparser' is missing.")

        cp = dcp.ConfigParser(
            interpolation=None, **self.duconfigparser_kwargs)
        with conf_error_wrapper():
            cp.read(conf_fpath)
            conf_dict = configparser_to_dict(cp, self._sect_sep)

        self.read_dict(conf_dict)


    def get(self, param, raw=None, con=None):
        '''
        Get the value for parameter `param`.  Parameter `param` can be consist
        of multiple components to address nested values.  Please consult the
        :py:meth:`duipoldict.InterpolDict.get` documentation for details.

        A not-``None`` switch `raw` forces whether the value is raw (for a
        ``True`` value) or expanded (for a ``False`` value).  For a ``None``
        value of `raw` the default is used.

        A not-``None`` switch `con` forces whether this dictionary has
        concrete values (for a ``True`` value) or abstract ones (for a
        ``False`` value).  For a ``None`` value of `con` the default is used.

        :return: the value for `param`.

        :raise: a :py:exc:`ConfigError` if getting of the 'param` value fails.
        '''
        with duipoldict_error_wrapper():
            val = super().get(param, raw, con)

        return val


    def __getitem__(self, param):
        '''
        Implementation of the :py:meth:`Config.__getitem__` method required
        for any :py:class:`Mapping` object.  Parameter `param` can be consist
        of multiple components to address nested values.  Please consult the
        :py:meth:`duipoldict.InterpolDict.__getitem__` documentation for
        details.

        :return: the value for `param`.

        :raise: a :py:exc:`ConfigError` if getting of the 'param` value fails.
        '''

        with duipoldict_error_wrapper():
            val = super().__getitem__(param)

        return val


    def expand_value(self, val):
        '''
        Expand raw value `val`, i.e. interpolate for all value references
        within `val` the respective values referred to.

        :return: expanded value for `val`.

        :raise: a :py:exc:`ConfigError` if expansion fails.
        '''
        with duipoldict_error_wrapper():
            exp_val = super().expand_value(val)

        return exp_val


def configparser_to_dict(cp, sect_sep=None):
    '''
    Export the configuration from a configuration parser `cp` with a similar
    API like :py:class:`configparser.ConfigParser` as (nested) dictionary.

    A not-``None`` string `sect_sep` is used as separator in section names to
    have nested sections for `cp`.

    :return: dictionary with the exported `cp` configuration.
    '''
    conf_dict = {}
    for s in cp.sections():
        # Get the options for the current section as dictionary.
        opts_vals_s = {o: cp[s][o] for o in cp.options(s)}

        # Calculate the components for the section name.
        sect_components_s = duipoldict.calc_key_components(s, sect_sep)

        # Add the options dictionary for the current section.
        _add_section_options_to_dict(
            conf_dict, sect_components_s, opts_vals_s)

    return conf_dict


def _add_section_options_to_dict(dct, sect_components, opts):
    '''
    Add options given by dictionary `opts` for section whose name components
    are given by sequence `sect_components` to dictionary `dict` using the
    section name components as nested keys.
    '''
    # Find the dictionary nested in `dct` described by section name components
    # `sect_components` that has to have last section name component as key.
    # Missing dictionaries on the way are created as empty ones.
    nested_dict = dct
    for s in sect_components[:-1]:
        if s not in nested_dict.keys():
            nested_dict[s] = {}
        nested_dict = nested_dict[s]

    # Now the options dictionary `opts` can be added to the found nested
    # dictionary with the last section name component as key.
    last_sect_component = sect_components[-1]
    if last_sect_component not in nested_dict:
        # No subsection is already added -> we can set options dictionary as
        # value for the last section name component as key.
        nested_dict[last_sect_component] = opts
    else:
        # A subsection is already added -> the options dictionary must update
        # the dictionary for the last section name component as key.
        nested_dict[last_sect_component].update(opts)


class ConfigError(Exception):
    '''
    Errors specific for configuration objects where string `msg` is the
    associated message, and `loc` an optional location information.
    '''
    def __init__(self, msg, loc=None):
        '''
        Initialize a :py:class:`ConfigError`.
        '''
        self.msg = msg
        self.loc = loc


    def __str__(self):
        '''
        :return: printable string representation of the exception.
        '''
        # Since we want to have full control whether the printable string for
        # the exception ends with a ``'.'`` or not we ensure first that there
        # is no ``'.'`` at the end.
        if self.msg.endswith('.'):
            ex_str = self.msg[:-1]
        else:
            ex_str = self.msg
        # When a location is given, we start a new line for it, but do not
        # want a ``'.'`` at the end.  Otherwise we ensure one.
        if self.loc is not None:
            ex_str += f" at:\n  {self.loc}"
        else:
            ex_str += '.'

        return ex_str


class error_wrapper(abc.ABC):
    '''
    Base class for context managers to wrap foreign :py:exc:`Error`s by
    :py:exc:`ConfigError`s.
    '''
    def __enter__(self):
        '''
        Method to enter this context.
        '''
        # Nothing to do when entering the context.
        pass


    @abc.abstractmethod
    def __exit__(self, exc_type, exc_obj, traceback):
        '''
        Method to exit this context, where for not-``None`` type `exc_type` is
        the type of raised exception, a not-``None`` `exc_obj` is the
        exception raised, and a not-``None`` `traceback` is the associated
        trace back.
        '''
        pass


class json_error_wrapper(error_wrapper):
    '''
    Wrapper a :py:exc:`json.JSONDecodeError` by a :py:exc:`ConfigError`.
    '''
    def __exit__(self, exc_type, exc_obj, traceback):
        if exc_type is json.JSONDecodeError:
            msg = 'The following error has occurred during reading\n'
            msg += 'the JSON configuration file'
            raise ConfigError(f"{msg}:\n  {exc_obj}")


class conf_error_wrapper(error_wrapper):
    '''
    Wrapper a :py:exc:`configparser.Error` by a :py:exc:`ConfigError`.
    '''
    def __exit__(self, exc_type, exc_obj, traceback):
        if exc_type is None:
            # We cannot pass ``None`` to ``issubclass``.
            pass
        elif issubclass(exc_type, configparser.Error):
            msg = 'The following error has occurred during reading\n'
            msg += 'the CONF configuration file'
            raise ConfigError(f"{msg}:\n  {exc_obj}")


class duipoldict_error_wrapper(error_wrapper):
    '''
    Wrapper for error exceptions raised in the :py:mod:`duipoldict` module.
    '''
    def __exit__(self, exc_type, exc_obj, traceback):
        if exc_type is None:
            # We cannot pass ``None`` to ``issubclass``.
            pass
        elif issubclass(exc_type, duipoldict.InterpolDictError):
            msg = 'The following error from `duipoldict` has occurred'
            raise ConfigError(f"{msg}\n  {exc_obj}")
        elif exc_type is KeyError:
            msg = 'The following key error has occurred'
            raise ConfigError(f"{msg}\n  {exc_obj}")
        elif exc_type is ValueError:
            msg = 'The following value error has occurred'
            raise ConfigError(f"{msg}\n  {exc_obj}")


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
