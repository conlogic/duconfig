..
 =============================================================================
 Title          : Generic configuration utilities

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2022-05-31

 Description    : Change log for package ``duconfig``.
 =============================================================================


=========
Changelog
=========


Version 5.0 (2022-05-31)
========================

- Simplified API for configuration file / directory paths:

  - New function :py:func:`duconfig.user_config_dir` to get the path of the
    user's XDG configuration directory.

  - Base the implementation of :py:func:`duconfig.search_config_file` on
    :py:func:`duconfig.user_config_dir` instead of
    :py:func:`duconfig.user_config_file`'s.

  - Remove function :py:func:`duconfig.user_config_file`: the path of a user
    configuration file can easily composed with
    :py:func:`duconfig.user_config_dir`.

- Improve documentation a bit:

  - Document usage of more :py:mod:`duconfig` stuff.

  - Use more Sphinx features for the documentation.

  - Make some formulations more precise.

  - Fix some documentation glitches.

- Add more built Python package stuff to file:`.gitignore`.


Version 4.0 (2022-04-28)
========================

- Generalize :py:func:`duconfig.calc_config_file` and rename it to
  :py:func:`duconfig.search_config_file`: allow a list of directories to
  search for the configuration file.

- Improve formatting of the Sphinx documentation a bit.

- Clean up for tests to rework.


Version 3.4 (2022-03-01)
========================

- New optional arguments `oth_begs` / `oth_ends` for
  :py:class:`duconfig.Config` for additional other begin / end delimiters
  passed to :py:mod:`duconfigparser`, too.

- Add another example (CONF configuration file + Python module with functions
  for value interpolation).

- Use the new example to test the new arguments `oth_begs` / `oth_ends` for
  :py:class:`duconfig.Config`, and to document their usage.

- Fix glitches found in usage documentation.


Version 3.3 (2022-02-17)
========================

- Enhance :py:func:`duconfig.calc_config_file`: Add an argument to control
  whether the fallback configuration file must exist if the user configuration
  file is missing.


Version 3.2 (2022-01-23)
========================

- Add two functions to get the paths of configuration files, together with
  tests and documentation of their usage:

  - :py:func:`duconfig.user_config_file` for the path of the user's
    configuration file,

  - :py:func:`duconfig.calc_config_file` for the path of the active
    configuration file.


Version 3.1 (2022-01-21)
========================

- Upgrade required version for ``duconfigparser`` to >= 2.3.

- Based on the new version for ``duconfigparser`` we can now use its new
  universal line continuation mechanism in CONF files.  For this an optional
  :py:class:`duconfig.Config` initialization parameter for the end marker of a
  line to be continued.  The CONF file examples, its tests and the
  documentation are updated for this new feature.


Version 3.0 (2021-12-29)
========================

- Wrap more :py:class:`duipoldict.InterpolDict` methods explicitly to have a
  precise documentation string for them - namely
  :py:meth:`duconfig.Config.write_dict` and
  :py:meth:`duconfig.Config.__getitem__`.  Ensure tests for these wrapper
  methods, too.

- Fix some test documentation glitches found.

- Complete documentation review for the new major version of ``duconfig``.
  Therefore the documentation is now complete for this version.


Version 3.0.dev10 (2021-12-27)
==============================

- Make the initialization arguments for :py:class:`duconfig.Config` explicit.

- Start documentation review for the new major version of ``duconfig``.

- Add support for Python 3.10.


Version 3.0dev9 (2021-10-28)
============================

- Upgrade required version for ``duconfigparser`` to >= 2.2.

- Fix parse errors that result from using the same separator for elements in
  list values in CONF files and for function arguments in function call
  references.  This is achieved by using the parameters for other delimiters
  that are new in version 2.2 of ``duconfigparser``.

- Adapt the example to profit from the fixed parsing, and adapt the tests
  to test the fix, and update the documentation.


Version 3.0dev8 (2021-10-11)
============================

- Upgrade required version for ``duconfigparser`` to >= 2.1.


Version 3.0dev7 (2021-10-01)
============================

- Upgrade required version for ``duipoldict`` to >= 0.5.1.
  This fixes an error message for function call references.


Version 3.0dev6 (2021-09-30)
============================

- Remove a leftover :py:func:`print` call from debugging.

- Fix to wrap a :py:exc:`duconfigparser.MissingListDelimError` as
  :py:exc:`duconfig.ConfigError`.


Version 3.0dev5 (2021-09-30)
============================

- Upgrade required version for ``duipoldict`` to >= 0.5.
  This way a :py:exc:`duipoldict.InterpolDictError` is raised if in a function
  call interpolation the number of arguments do not match the function's
  arity.

- Upgrade required version for ``duconfigparser`` to >= 2.1dev1.
  This way a :py:exc:`duconfigparser.MissingListDelimError` is raised if a
  value string has a proper list begin or end delimiter, but not the opposite
  one.

- Catch errors when trying read non-UTF8 configuration files, and add tests
  for this.


Version 3.0dev4 (2021-09-28)
============================

- Upgrade required version for ``duipoldict`` to >= 0.4.

- Proof-read the code documentation.


Version 3.0dev3 (2021-09-27)
============================

- Upgrade required version for ``duipoldict`` to >= 0.3, and adapt
  ``duconfig`` the API and terminological changes of ``duipoldict``.
  This fixes configuration parsing errors, and provides much improved debug
  logging and error messages.

- Use a simplified example from newer Shinobi for the example files.

- Start to update the documentation.

- Test all syntax parameters by alternative syntax, and fix the name of one
  syntax parameter.


Version 3.0dev2 (2021-09-12)
============================

- Update required version for ``duconfigparser`` to >= 2.0.

- Use semantic versioning for ``duconfigparser``, too.

- Fix a file:`README.rst` typo.


Version 3.0dev1 (2021-09-08)
============================

- Use package ``duipoldict`` instead of ``dumapinterpol`` for value
  interpolation.  For this ``duipoldict`` (currently version >= 0.1dev5)
  becomes a dependency of ``duconfig`` instead of ``dumapinterpol``.

- Base :py:class:`duconfig.Config` objects on
  :py:class:`duipoldict.InterpolDict` objects from package ``duipoldict``.
  This has (at least) the following consequences for
  :py:class:`duconfig.Config`:

  - Configuration data is now internally stored with tokenized values.

  - :py:class:`duconfig.Config` profits from the enhanced functionality of
    :py:class:`duipoldict.InterpolDict` like proper value parsing with much
    improved  error messages, or additional (like
    :py:meth:`duipoldict.InterpolDict.read_dict`) or enhanced (like
    :py:meth:`duipoldict.InterpolDict.get`) methods.

  - Nevertheless :py:class:`duconfig.Config` becomes smaller since parts of its
    functionality is now already implemented in
    :py:class:`duipoldict.InterpolDict`.

- Required version for dependency package ``dubasiclog`` is updated to >=
  1.3.  So we gain, for instance, better control for debug logging.


Version 2.0.1 (2021-08-07)
==========================

- Adapt examples to versions >= 2.0 of Shinobi.

- Ease test data generation by adding a Python script to do it.


Version 2.0 (2021-08-07)
========================

- Improve tests a lot:

  - Remove redundant tests that already done for ``dumapinterpol`` or
    ``duconfigparser``.

  - Consolidate test case classes - only one class per method.

  - Add doc strings to all test methods.

  - Add tests for expected errors initially caused by foreign exceptions.

- Wrap foreign exceptions raised as errors by :py:exc:`duconfig.ConfigError`
  objects.


Version 1.1.2 (2021-07-22)
==========================

- Test modules get their directory paths more simple.


Version 1.1.1 (2021-07-05)
==========================

- Make tests more robust.


Version 1.1 (2021-07-05)
========================

- Required version for optional dependency package ``duconfigparser`` is
  updated to >= 1.1.  This way we can use the backward compatible API from
  ``duconfigparser`` for CONF configuration files, too.

- Add a test using a CONF configuration file with alternative syntax.


Version 1.0.2 (2021-07-04)
==========================

- Migrate package itself to :py:mod:`pathlib` and ``f"..."``-strings.

- Fix `.gitignore` file.


Version 1.0.1 (2021-07-04)
==========================

- Fix development status (via ``classifiers`` for ``setup``).

- Drop support for Python 3.5, since ``f"..."``-strings are used (via
  ``classifiers`` for ``setup``).

- Switch to semantic versions for the dependencies.

- Use more :py:mod:`pathlib` in the package tools.


Version 1.0 (2021-06-11)
========================

- Add real Sphinx-based documentation.

- Make :file:`CHANGELOG.rst`-formatting Sphinx-compatible.

- Migrate package tools to ``f"..."``-strings and :py:mod:`pathlib`.

- Update versions for dependencies, and adapt to the new list value
  syntax in CONF files.

- Add simple configuration file examples and unit tests.


Version 0.4 (2021-02-08)
========================

- Make updating using nested keys more robust.

- Add priority items to :py:class:`duconfig.Config` initialization.


Version 0.3.1 (2021-01-31)
==========================

- Update required version of ``dumapinterpol`` to >= 0.1.1 for improved
  :py:exc:`dumapinterpol.InterpolationError`.

- Improve :py:exc:`duconfig.ConfigError` (fix documentation and improve its
  :py:meth:`duconfig.ConfigError.__str__` method's logic).


Version 0.3 (2021-01-29)
========================

- Before reading a configuration file, check that there is a readable file.

- Remove methods :py:meth:`duconfig.ConfigError.read_json_file` and
  :py:meth:`duconfig.ConfigError.read_conf_file` from the interface of
  :py:meth:`duconfig.Config`.


Version 0.2 (2021-01-28)
========================

- Implement to set values using the ``[]`` operator; even for compound
  parameters.


Version 0.1 (2021-01-12)
========================

Initial version of the package:

- Handle configuration files in JSON or extended CONF format (for the latter
  package ``duconfigparser`` is needed).

- Provide value interpolation in mappings for key and function call
  references.

..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:

..  LocalWords:  Shinobi
