# ============================================================================
# Title          : Generic configuration utilities
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-08-08
#
# Description    : See documentation string below.
#
# License        : LGPL3 <https://www.gnu.org/licenses/lgpl-3.0.en.html>.
#
# Copyright      : 2021 Dirk Ullrich <dirk.ullrich@posteo.de>
# ============================================================================

'''
Single module for package ``duconfig``.
'''


import collections as colls
import json
import configparser
import pathlib
import os
import abc

import dumapinterpol
import dubasiclog
try:
    import duconfigparser
    has_conf_support = duconfigparser.version >= '1.1'

except ModuleNotFoundError:
    has_conf_support = False


# Parameters
# ==========

# Version of the very package.
version = '2.0.1'


# Functions and classes
# =====================


# For uniform logging we use the module-global logging facilities from package
# ``dubasiclog``.
enable_debug_log = dubasiclog.enable_debug_log
debug_log = dubasiclog.debug_log


class Config(colls.UserDict):
    '''
    Mapping object to store configuration read from appropriate sources with
    value interpolation allowed for its values.

    The following types of value interpolation can be used:

    + key reference interpolation: references to other keys within the
      configuration will be replaced by their corresponding values.  For this
      interpolation `key_beg` / `key_end` is used as begin /end delimiter for
      key references, and `key_sep` as separator between key components.

    + function call reference interpolation: references to a function call
      using the name of a function available for interpolation are replaced by
      the value of the respective function call.  Here the following
      parameters are relevant: `fct_beg` / `fct_end` as delimiter for the
      begin / end of function call reference, `args_sep` as separator between
      arguments, and `fcts` as list of functions available for the
      interpolated function calls.

    For details about interpolated please consult the documentation of
    :py:mod:`dumapinterpol` from the package with the same name.

    Parameters `list_beg`, `list_sep` and `list_end` determine the handling of
    list value strings in configuration files of the CONF format.  Please
    lookup the details in the documentation of :py:mod:`duconfigparser` from
    the package with the same name.

    A ``True`` value for `raw` causes to use raw values by default; otherwise
    interpolated values are used by default.

    For a not-``None`` list `prio_items` of key value pairs, where for a
    not-``None`` `key_sep` the key can be a nested one, these key value pairs
    are used within the configuration object irrespective of any read
    configuration files.  Note that the values in these `prio_items` pairs can
    only be string values.
    '''
    def __init__(self, key_beg='%{', key_end='}%', key_sep='.',
                 fct_beg='%(', fct_end=')%', args_sep=',', fcts=[],
                 list_beg='[', list_sep=',', list_end=']',
                 raw=True, prio_items=[]):
        '''
        Initialize a configuration object.
        '''
        self.key_sep = key_sep
        self.list_beg = list_beg
        self.list_sep = list_sep
        self.list_end = list_end
        self.raw = raw
        self.prio_items = prio_items

        super().__init__()

        # Interpolation used for the configuration values.
        self._interpolation = dumapinterpol.MapInterpolation(
            key_beg, key_end, key_sep, fct_beg, fct_end, args_sep, fcts)


    def get(self, param, raw=None):
        '''
        Get value for configuration parameter `param`.

        A not-``None`` value `raw` forces whether the value is interpolated
        (for a ``True`` value) or not (for a ``False`` value).

        :return: the value of `param`.

        :raise: a :py:exc:`ConfigError` if value lookup or interpolation
                fails.
        '''
        if raw is None:
            raw = self.raw

        with key_error_wrapper(), interpolation_error_wrapper():
            val = self._interpolation.get(self.data, param, raw)

        return val


    def __getitem__(self, param):
        '''
        Implementation of the :py:meth:`__getitem__` method required for any
        :py:class:`Mapping` object.

        :raise: a :py:exc:`ConfigError` if value lookup or interpolation
                fails.
        '''

        return self.get(param, raw=self.raw)


    def _set(self, param, val):
        '''
        Set the value for parameter `param` to `val`.  For a not-``None`` key
        separator of ``self``, `param` can be a nested key.
        '''
        if self.key_sep is None:
            val = self.data[param]
        else:
            param_parts = param.split(self.key_sep)
            last_param = param_parts[-1]
            params_head = param_parts[:-1]
            last_mapg = self.data
            for p in params_head:
                last_mapg = last_mapg[p]
            last_mapg[last_param] = val


    def __setitem__(self, param, val):
        '''
        Implementation of the :py:meth:`__setitem__` method required for any
        :py:class:`MutableMapping` object.

        :raise: a :py:exc:`ConfigError` if value setting fails.
        '''
        with key_error_wrapper():
            self._set(param, val)


    def interpolate_value(self, val):
        '''
        :return: interpolated value of configuration value `val`.

        :raise: a :py:exc:`ConfigError` if interpolation fails.
        '''
        with key_error_wrapper(), interpolation_error_wrapper():
            val = self._interpolation.interpolate_value(val, self.data)

        return val


    def _read_json_file(self, json_fpath):
        '''
        Read JSON configuration file with path `json_fpath`, and store the
        resulting configuration.

        :raise: an :py:exc:`ConfigError` if the configuration file could not
                be read as a JSON file.
        '''
        with open(json_fpath, 'r') as json_fobj, json_error_wrapper():
            json_cfg = json.load(json_fobj)

        self.data.update(json_cfg)


    def _read_conf_file(self, conf_fpath):
        '''
        Read CONF configuration file with path `conf_fpath`, and store the
        resulting configuration.

        Please note that this method is only available if the module
        :py:mod:`duconfigparser` could be loaded.

        :raise: an :py:exc:`ConfigError` if :py:mod:`duconfigparser` could not
                be loaded, or if the configuration file could not be read as a
                CONF file.
        '''
        if not has_conf_support:
            raise ConfigError("Module 'duconfigparser' is missing.")

        cp = duconfigparser.DUConfigParser(
            list_beg=self.list_beg, list_sep=self.list_sep,
            list_end=self.list_end, interpolation=None)
        with conf_error_wrapper():
            cp.read(conf_fpath)
        flat_cfg_dict = configparser_to_dict(cp, self.key_sep)
        nest_cfg_dict = flat_dict_to_nested(flat_cfg_dict)
        self.data.update(nest_cfg_dict)


    def read_file(self, fpath):
        '''
        Read configuration file with path `fpath`, if the file's format can be
        handled, and store the resulting configuration.

        :raise: an :py:exc:`ConfigError` if reading fails in any way.
        '''
        fpath = pathlib.Path(fpath)
        if not fpath.is_file():
            raise ConfigError(
                f"No readable configuration file at:\n  {fpath}")

        fext_with_sep = fpath.suffix
        fext = fext_with_sep.lstrip(os.extsep).lower()
        if fext == 'json':
            self._read_json_file(fpath)
        elif fext == 'conf':
            self._read_conf_file(fpath)
        else:
            # We add information for the available file formats and their
            # extensions.
            msg = f"Format for configuration file at\n  {fpath}\n"
            msg += 'is wrong.  The following formats are allowed:'
            msg += "\n- JSON format (file name '*.json')"
            if has_conf_support:
                msg += "\n- CONF format (file name '*.conf')"
            raise ConfigError(msg)

        # Finally we have apply any given priority items.
        for (k, v) in self.prio_items:
            if self.key_sep is not None:
                k_as_tuple = tuple(k.split(self.key_sep))
            else:
                k_as_tuple = (k,)
            _add_key_val_nested(k_as_tuple, v, self.data)


def configparser_to_dict(cp, sect_sep=None):
    '''
    Export items from :py:mod:`configparser`-like `cp` as dictionary.

    A not-``None`` string `sect_sep` is used as separator for section parts if
    nested sections are used for `cp`.

    :return: dictionary with the exported `cp` items.
    '''
    cp_items = {}
    for s in cp.sections():
        opts_vals_s = {o: cp[s][o] for o in cp.options(s)}
        if sect_sep is not None and sect_sep in s:
            # Nested section -> use it converted in a tuple as key.
            key_s = tuple(s.split(sect_sep))
        else:
            # Plain section -> use itself as key.
            key_s = s
        cp_items[key_s] = opts_vals_s

    return cp_items


def flat_dict_to_nested(xs):
    '''
    Convert flat dictionary `xs` to a corresponding nested one.

    :return: converted nested dictionary.
    '''
    nest_xs = {}
    for k in xs.keys():
        debug_log(f"->Key '{k}'")
        if isinstance(k, tuple):
            k_as_tuple = k
        else:
            k_as_tuple = (k,)
        _add_key_val_nested(k_as_tuple, xs[k], nest_xs)

    return nest_xs


def _add_key_val_nested(ks, v, xs):
    '''
    Add for structured key `ks` the value `v` to nested dictionary `xs`.
    '''
    # Navigate in the nested dictionary to the part dictionary that gets
    # the current value added.
    debug_log('---Value to add for key---')
    debug_log(str(v))
    nest_ks = xs
    for k in ks[:-1]:
        # If the nested dictionary has no part dictionary for the current
        # key component, we add an empty one for this key.
        if k not in nest_ks:
            nest_ks[k] = {}
        nest_ks = nest_ks[k]
    # Add value `v` within the last found / added part dictionary with the
    # last key component.
    last_k = ks[-1]
    if last_k not in nest_ks.keys():
        # There is no old value for the last key component -> simply set the
        # value to `v`.
        debug_log('-->Set value for last key component anew.')
        nest_ks[last_k] = v
    else:
        # There is an old value for the last key component.
        debug_log('---Value (old) for last key component---')
        debug_log(str(nest_ks[last_k]))
        v_is_map = isinstance(v, colls.abc.Mapping)
        if v_is_map and isinstance(nest_ks[last_k], dict):
            # Both value `v` is a mapping and the old value for the last key
            # component is a dictionary -> update the old value by `v`.
            debug_log('-->Update old value.')
            nest_ks[last_k].update(v)
        else:
            # Otherwise we set the old value to `v`.
            debug_log('-->Set old value.')
            nest_ks[last_k] = v
    debug_log('---Value (new) for last key component---')
    debug_log(str(nest_ks[last_k]))
    debug_log('----------------------------------------')


class ConfigError(Exception):
    '''
    Errors specific for configuration objects where string `msg` is the
    associated message, and `loc` an optional location information.
    '''
    def __init__(self, msg, loc=None):
        '''
        Initialize a :py:class:`ConfigError`.
        '''
        self.msg = msg
        self.loc = loc


    def __str__(self):
        '''
        :return: printable string representation of the exception.
        '''
        # Since we want to have full control whether the printable string for
        # the exception ends with a ``'.'`` or not we ensure first that there
        # is no ``'.'`` at the end.
        if self.msg.endswith('.'):
            ex_str = self.msg[:-1]
        else:
            ex_str = self.msg
        # When a location is given, we start a new line for it, but do not
        # want a ``'.'`` at the end.  Otherwise we ensure one.
        if self.loc is not None:
            ex_str += f" at:\n  {self.loc}"
        else:
            ex_str += '.'

        return ex_str


class error_wrapper(abc.ABC):
    '''
    Base class for context managers to wrap foreign :py:exc:`Error`s by
    :py:esc:`ConfigError`s.
    '''
    def __enter__(self):
        '''
        Method to enter this context.
        '''
        # Nothing to do when entering the context.
        pass


    @abc.abstractmethod
    def __exit__(self, exc_type, exc_obj, traceback):
        '''
        Method to exit this context, where for not-``None`` type `exc_type` is
        the type of raised exception, a not-``None`` `exc_obj` is the
        exception raised, and a not-``None`` `traceback` is the associated
        trace back.
        '''
        pass


class json_error_wrapper(error_wrapper):
    '''
    Wrapper a :py:exc:`json.JSONDecodeError` by a :py:exc:`ConfigError`.
    '''
    def __exit__(self, exc_type, exc_obj, traceback):
        if exc_type is json.JSONDecodeError:
            msg = 'The following error has occurred during reading\n'
            msg += 'the JSON configuration file'
            raise ConfigError(f"{msg}:\n  {exc_obj}")


class conf_error_wrapper(error_wrapper):
    '''
    Wrapper a :py:exc:`configparser.Error` by a :py:exc:`ConfigError`.
    '''
    def __exit__(self, exc_type, exc_obj, traceback):
        if exc_type is None:
            # We cannot pass ``None`` to ``issubclass``.
            pass
        elif issubclass(exc_type, configparser.Error):
            msg = 'The following error has occurred during reading\n'
            msg += 'the CONF configuration file'
            raise ConfigError(f"{msg}:\n  {exc_obj}")


class key_error_wrapper(error_wrapper):
    '''
    Wrapper a :py:exc:`KeyError` by a :py:exc:`ConfigError`.
    '''
    def __exit__(self, exc_type, exc_obj, traceback):
        if exc_type is KeyError:
            raise ConfigError(
                f"Missing key '{exc_obj.args[0]}'.")


class interpolation_error_wrapper(error_wrapper):
    '''
    Wrapper a :py:exc:`dumapinterpol.InterpolationError` by a
    :py:exc:`ConfigError`.
    '''
    def __exit__(self, exc_type, exc_obj, traceback):
        if exc_type is dumapinterpol.InterpolationError:
            msg = 'The following interpolation error has occurred'
            raise ConfigError(f"{msg}:\n  {exc_obj}")


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
