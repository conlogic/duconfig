# ============================================================================
# Title          : Generic configuration utilities
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2022-04-28
#
# Description    : See documentation string below.
# ============================================================================

'''
Unit tests for module :py:mod:`duconfig`.
'''


import pathlib
import json
import unittest
import sys

import duconfig


# Functions and classes
# =====================


def conv_tuple_to_str_key(key_tuple, key_sep):
    '''
    Convert a flat dictionary key given as tuple `key_tuple` to corresponding
    string key using key separator `key_sep`.
    '''
    if isinstance(key_tuple, tuple):
        k_str = ''
        for e in key_tuple:
            if len(k_str) > 0 and key_sep is not None:
                k_str += key_sep
            k_str += str(e)
    else:
        k_str = key_tuple

    return k_str


def conv_nested_to_flat_tuple_dict(nest_dict):
    '''
    :return: nested dictionary `nested_dict` converted to a flat dictionary
             with tuple keys and the same values for corresponding keys.
    '''
    flat_dict = {}
    for k in nest_dict.keys():
        if isinstance(nest_dict[k], dict):
            flat_dict_k = conv_nested_to_flat_tuple_dict(nest_dict[k])
            for flat_k in flat_dict_k.keys():
                new_flat_k = (k,) + flat_k
                flat_dict[new_flat_k] = flat_dict_k[flat_k]
        else:
            flat_dict[(k,)] = nest_dict[k]

    return flat_dict


def conv_nested_to_flat_str_dict(nest_dict, key_sep='.'):
    '''
    :return: nested dictionary `nested_dict` converted to a flat dictionary
             with structured string keys using key separator `key_sep` and the
             same values for corresponding keys.
    '''
    flat_tuple_dict = conv_nested_to_flat_tuple_dict(nest_dict)
    flat_str_dict = {
        conv_tuple_to_str_key(k, key_sep): v
        for (k, v) in flat_tuple_dict.items()
    }

    return flat_str_dict


def read_json_file(fpath):
    '''
    :return: content of JSON file with path `fpath` read into a dictionary.
    '''
    with open(fpath, 'rt') as fobj:
        content = json.load(fobj)

    return content


def write_json_file(content, fpath, indent=1):
    '''
    Write content `content` to JSON file with path `fpath` using indentation
    level `indent`.
    '''
    with open(fpath, 'wt') as fobj:
        json.dump(content, fobj, indent=indent)


def mk_test_data():
    '''
    Create test data from the example configuration.

    :return: list of all generated test data files.
    '''
    data_fpaths = []

    tc = base_Config_TC()
    tc.setUp()

    # Flat dictionary with raw values.
    nest_raw_dict = read_json_file(tc.json_fpath)
    flat_raw_dict = conv_nested_to_flat_str_dict(nest_raw_dict)
    write_json_file(flat_raw_dict, tc.flat_raw_fpath)
    data_fpaths.append(tc.flat_raw_fpath)

    # Nested dictionary with interpolated values.
    cfg = duconfig.Config(fcts=tc.fcts, raw=False)
    cfg.read_file(tc.conf_fpath)
    nest_itp_dict = {k: v for (k, v) in cfg.items()}
    write_json_file(nest_itp_dict, tc.nest_itp_fpath)
    data_fpaths.append(tc.nest_itp_fpath)

    # Flat dictionary with interpolated values.
    flat_itp_dict = conv_nested_to_flat_str_dict(nest_itp_dict)
    write_json_file(flat_itp_dict, tc.flat_itp_fpath)
    data_fpaths.append(tc.flat_itp_fpath)

    # Flat dictionary with raw values with priority items added.
    cfg = duconfig.Config(fcts=tc.fcts, prio_items=tc.prio_items, raw=True)
    cfg.read_file(tc.conf_fpath)
    prio_raw_dict = conv_nested_to_flat_str_dict(
        {k: v for (k, v) in cfg.items()})
    write_json_file(prio_raw_dict, tc.prio_raw_fpath)
    data_fpaths.append(tc.prio_raw_fpath)

    # Flat dictionary with interpolated values with priority items added.
    cfg = duconfig.Config(fcts=tc.fcts, prio_items=tc.prio_items, raw=False)
    cfg.read_file(tc.conf_fpath)
    prio_itp_dict = conv_nested_to_flat_str_dict(
        {k: v for (k, v) in cfg.items()})
    write_json_file(prio_itp_dict, tc.prio_itp_fpath)
    data_fpaths.append(tc.prio_itp_fpath)

    return data_fpaths


class base_Config_TC(unittest.TestCase):
    '''
    Base class for tests related to :py:class:`Config` objects.
    '''
    def setUp(self):
        # Directory path of this very module.
        this_dpath = pathlib.Path(__file__).parent
        # Path for the directory containing the examples.
        self.examples_dpath = this_dpath.parent / 'examples'
        # Path for the configuration CONF example file.
        self.conf_fpath = self.examples_dpath / 'config.conf'
        # Path for the configuration JSON example file.
        self.json_fpath = self.examples_dpath / 'config.json'
        # List with functions available for configuration example
        # interpolation.
        self._orig_sys_path = sys.path
        sys.path.append(str(self.examples_dpath))
        import config as fcts
        self.fcts = fcts.CONFIG_FCTS
        # Path for the directory with test data.
        self.test_data_dpath = this_dpath / 'data'
        # Paths for test data files representing configuration example files.
        # - With raw values for a flat dictionary.
        self.flat_raw_fpath = self.test_data_dpath \
            / 'config_flat_raw.json'
        # - With interpolated values for a flat dictionary.
        self.flat_itp_fpath = self.test_data_dpath \
            / 'config_flat_itp.json'
        # - With interpolated values for a nested dictionary.
        self.nest_itp_fpath = self.test_data_dpath \
            / 'config_nest_itp.json'
        # - With raw values for a flat dictionary with priority items added.
        self.prio_raw_fpath = self.test_data_dpath \
            / 'config_prio_raw.json'
        # - With interpolated values for a flat dictionary with priority items
        # added.
        self.prio_itp_fpath = self.test_data_dpath \
            / 'config_prio_itp.json'

        # Specific example parameters.
        # - Parameter with value without interpolation.
        self.raw_val_param = 'paths.exe'
        # - Parameter with value with interpolation.
        self.itp_val_param = 'paths.obns_mod'
        # - Old parameter set in a priority item without interpolation.
        self.raw_val_old_prio_param = 'env.basedir'
        # - New parameter set in a priority item without interpolation.
        self.raw_val_new_prio_param = 'env.isostd'
        # - Old parameter set in a priority item whose interpolation is
        #   affected by another parameter set in a priority item, too.
        self.itp_val_old_prio_param = 'vars.cxxopts'
        # Priority items to be used with the example configuration.
        self.prio_items = [
            (self.raw_val_old_prio_param, '/tmp/tests'),
            (self.raw_val_new_prio_param, 'c++17'),
            (self.itp_val_old_prio_param,
             "-c -Wall -pedantic -std=%{env.isostd}%")
        ]

        # A string readable by a :py:class:`configparser` provoking a key
        # error during interpolation.
        self.key_error_conf_str = '''
        [sect]
        opt = %{sect.foo}%
        '''


    def tearDown(self):
        # Restore original module load path.
        sys.path = self._orig_sys_path


class Config_setitem_TC(base_Config_TC):
    '''
    Tests of method :py:meth`__setitem__` of :py:class:`Config`.
    '''
    def setUp(self):
        super().setUp()
        # A :py:class:`Config` object initialized with defaults but functions
        # for interpolation, and the example configuration read.
        self.std_cfg = duconfig.Config(fcts=self.fcts)
        self.std_cfg.read_file(self.json_fpath)
        # Specific example parameters and values.
        # - Flat key.
        self.flat_key = 'vars'
        # - Nested key.
        self.nest_key = self.flat_key + self.std_cfg.key_sep + 'cxxopts'
        # - New value for nested key.
        self.nest_val = '-c -Werror -Wall -pedantic --std=c++14'
        # - New value for flat key.
        self.flat_val = {self.nest_key: self.nest_val}


    def test_key_error(self):
        '''
        Test with a :py:exc:`KeyError`.
        '''
        with self.assertRaises(duconfig.ConfigError):
            self.std_cfg['foo.bar'] = 'baz'


    def test_flat_key(self):
        '''
        Test for a key with 1 component.
        '''
        self.std_cfg[self.flat_key] = self.flat_val
        self.assertDictEqual(self.std_cfg[self.flat_key], self.flat_val)


    def test_nest_key(self):
        '''
        Test for a key with components.
        '''
        self.std_cfg[self.nest_key] = self.nest_val
        self.assertSequenceEqual(self.std_cfg[self.nest_key], self.nest_val)


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
