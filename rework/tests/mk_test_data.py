#!/usr/bin/env python3
# ============================================================================
# Title          : Generic configuration utilities
#
# Classification : Python script
#
# Author         : Dirk Ullrich
#
# Date           : 2021-08-08
#
# Description    : See documentation string below.
# ============================================================================

'''
Test helper script to generate all test data.
'''


import sys
import pathlib
this_dpath = pathlib.Path(__file__).parent
pkg_dpath = this_dpath.parent
sys.path.append(str(pkg_dpath))

import tests.test_duconfig


# Functions
# =========


def main():
    print("Generating test data for 'duconfig'...")
    fpaths = tests.test_duconfig.mk_test_data()
    print("Test data files for 'duconfig' generated:")
    for f in fpaths:
        print(f"  {f}")


# Doing
# =====

if __name__ == '__main__':
    main()


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
