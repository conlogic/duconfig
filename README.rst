..
 =============================================================================
 Title          : Generic configuration utilities

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2022-04-28

 Description    : Read me file for the package.
 =============================================================================


.. _read_me:

=============================================
``duconfig``: Generic configuration utilities
=============================================

This package provides configurations with expressive syntax for several
configuration file formats.  By default, configuration files in JSON format
are supported.  If the package `duconfigparser`_ is available, too,
configuration files in (an extend form [#conflistvals]_ of) the CONF format
are supported, too.

For both formats you can use value interpolation, based on the package
`duipoldict`_.  This value interpolation allows to refer, within the values of
configuration parameters, to other values.  Details for value interpolation
you can find in the `duipoldict documentation`_.

The ``examples`` subdirectory of the repository has examples for a simple
configuration.

One example is a build configuration for build tool `Shinobi`_ that uses this
very package.  This build configuration is given both in :download:`JSON
<../examples/shinobi_config.json>` and :download:`CONF
<../examples/shinobi_config.conf>` format -- together with a
:download:`Python module <../examples/shinobi_config.py>` that defines the
functions used for value interpolation within these files.

A second example is configuration for a setup tool.  This setup configuration
is provided in :download:`CONF <../examples/ninja_setup.conf>` format, again
together with a :download:`Python module <../examples/ninja_setup.py>`
defining functions used for value interpolation in this CONF file.


.. [#conflistvals] For instance, the CONF files can have list values.  The list
                   syntax for list values in CONF format files is described in
                   the `duconfigparser documentation`_.

.. _duconfigparser: https://gitlab.com/conlogic/duconfigparser
.. _duconfigparser documentation: https://duconfigparser.readthedocs.io
.. _duipoldict: https://gitlab.com/conlogic/duipoldict
.. _duipoldict documentation: https://duipoldict.readthedocs.io
.. _Shinobi: https://gitlab.com/conlogic/shinobi

..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:

..  LocalWords:  duconfigparser
